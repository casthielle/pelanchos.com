<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [ 'name', 'image', 'email', 'phone_number', 'address', 'owner' ];

    /**
     * Company has many Subsidiaries.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subsidiaries(){

    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = company_id, localKey = id)
    	return $this->hasMany(Subsidiary::class);
    }
}
