<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function(){
	Route::auth();
	Route::get('/', 'IndexController@index');
	Route::get('/admin', 'Admin\HomeController@index');
	Route::get('/notifications', 'Admin\ReservationsController@notifications');
	Route::post('savebasic', 'Admin\UsersController@basic');
    Route::post('savecontact', 'Admin\UsersController@contact');
	Route::post('change', 'Admin\UsersController@change');
	Route::post('reserve', 'IndexController@reserve');
	Route::post('changeimage/{id}', 'Admin\UsersController@image');
	Route::resource('users', 'Admin\UsersController', 				['only' => ['index', 'store', 'update', 'destroy', 'show']]);
	Route::resource('reservations', 'Admin\ReservationsController', ['only' => ['index', 'store', 'update', 'destroy', 'show']]);
	Route::resource('categories', 'Admin\CategoriesController', 	['only' => ['index', 'store', 'update', 'destroy', 'show']]);
	Route::resource('items', 'Admin\ItemsController', 				['only' => ['index', 'store', 'update', 'destroy', 'show']]);
	Route::resource('startup', 'Admin\StartupController', 			['only' => ['index', 'store', 'update', 'destroy', 'show']]);
	Route::resource('events', 'Admin\EventsController', 			['only' => ['index', 'store', 'update', 'destroy', 'show']]);
});

Route::group(['middleware' => 'api'], function(){
	Route::post('/auth', 'ApiController@auth');
	Route::post('/register', 'ApiController@register');
});


