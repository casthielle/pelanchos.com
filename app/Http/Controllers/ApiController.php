<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests;

use Validator;
use Response;
use Auth;

use Carbon\Carbon;

use App\User;

class ApiController extends Controller{


	public function __construct(){
        date_default_timezone_set("America/Caracas");
    }

	public function auth(Request $request){
		$user = User::where('email', $request->get('email'))->first();
		if($user != NULL){
			if(Hash::check($request->get('password'), $user->password)){
				$user->error = 0;
        		return Response::json($user);
			} else { return Response::json(['error' => 1, 'details' => 'La contraseña es incorrecta.']); }
		} else { return Response::json(['error' => 1, 'details' => 'El usuario no existe.']); }
	}




	public function register(Request $request){
		$validate = Validator::make($request->all(), [
            'first_name'      =>  'required|alpha',
            'last_name'    =>  'required|alpha',
            'email'       =>  'required|email|unique',
            'password'    =>  'required|confirmed|min:8'
        ]);

        $errors = $validate->fails();
        if (!$errors){
            $user = User::create([
                'first_name'    =>  $request->get('first_name'),
                'last_name'     =>  $request->get('last_name'),
                'email'         =>  $request->get('email'),
                'password'      =>  Hash::make($request->get('password')),
                'type'          =>  'user',
                'updated_at'    =>  date('Y-m-d H:i:s'),
                'created_at'    =>  date('Y-m-d H:i:s')
            ]);
			$user->error = 0;
    		return Response::json($user);
        }
		else{
    		return Response::json(['error' => 1, 'details' => $validate->errors() ]);
		}
	}
}
