<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Validator;
use Response;
use Auth;

use App\Http\Requests;
use App\MenuItem as Menu;
use App\Company;
use App\Subsidiary;
use App\Reservation;
use App\Event;

class IndexController extends Controller
{
	public function __construct(){

	}

    public function index(){

    	$menu = Menu::all();

    	if($menu->count()>12){
    		$ramdom = $menu->random(12);
            $menu = $ramdom->all();
    	}

        $company = Company::first();
        $subsidiaries = Subsidiary::all();

        $events = Event::all();

        foreach ($events as $event) {
            $event->subsidiary;
            $event->date = Carbon::createFromFormat('Y-m-d', $event->date)->format('j-M');
        }

        return view('index', compact('menu', 'company', 'subsidiaries', 'events'));
    }

    public function reserve(Request $request){

        if ($request->ajax()){

            $validate = Validator::make($request->all(), [
                'name'          =>  'required',
                'email'         =>  'required|email',
                'phone_number'  =>  'required',
                'table_size'    =>  'required|integer',
                'date'          =>  'required|date_format:m/d/Y',
                'book_type'     =>  'required',
                'subsidiary_id' =>  'required'
            ]);

            $error = $validate->fails();
            if (!$error){
                $date = Carbon::createFromFormat('m/d/Y', $request->input('date'));
                $reservation = Reservation::create([
                    'name'          =>  $request->input('name'),
                    'email'         =>  $request->input('email'),
                    'phone_number'  =>  $request->input('phone_number'),
                    'table_size'    =>  $request->input('table_size'),
                    'date'          =>  $date,
                    'book_type'     =>  $request->input('book_type'),
                    'subsidiary_id' =>  $request->input('subsidiary_id'),
                    'status'        =>  'on hold',
                    'requirements'  =>  $request->input('requirements'),
                    'updated_at'    =>  date('Y-m-d H:i:s'),
                    'created_at'    =>  date('Y-m-d H:i:s')
                ]);
                $json = [
                    'type'     => 'success',
                    'message'  => 'The table has been booked successfully.',
                    'title'    => 'Success!'
                ];
                return Response::json($json);
            }

            return Response::json($validate->errors());
        }
    }
}
