<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use Validator;
use Response;
use Auth;

use Storage;
use File;
use Input;
use Image;

use App\MenuGroup as Category;
use App\MenuItem as Item;

class ItemsController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request){
        //date_default_timezone_set("America/Caracas");
        $this->middleware('auth');
        $this->middleware('ajax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $items = Items::orderBy('updated_at', 'desc')->get();
        return Response::json($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validate = Validator::make($request->all(), [
            'english_name'  =>  'required',
            'image'         =>  'required|image',
            'position'      =>  'numeric',
            'price'         =>  'numeric',
            'offer'         =>  'numeric',
        ]);

        $error = $validate->fails();
        if (!$error){

            $file = Input::file('image');
            $path = public_path('storage/images/items');
            $image = Image::make(Input::file('image'));

            $image_name = str_replace(' ', '_', $request->input('english_name')).'.'.$file->getClientOriginalExtension();
            $height = $image->height();
            $width = $image->width();
            $image->backup();
            $image->save($path.'/'.$image_name);
            if($height < $width){ $image->heighten(80); }
            else{ $image->widen(80); }
            $image->crop(80,80);
            $image->save($path.'/avatar_'.$image_name);
            $image->reset();
            if($height < $width){ $image->heighten(130); }
            else{ $image->widen(130); }
            $image->crop(130,130);
            $image->save($path.'/preview_'.$image_name);
            $image->crop(300,150);
            $image->save($path.'/card_'.$image_name);
            $image->reset();
            if($height < $width){ $image->heighten(500); }
            else{ $image->widen(500); }
            $image->crop(500,500);
            $image->save($path.'/thumb_'.$image_name);

            $item = Item::create([
                'english_name'  =>  $request->input('english_name'),
                'spanish_name'  =>  $request->input('spanish_name'),
                'image'         =>  $image_name,
                'description'   =>  $request->input('description'),
                'position'      =>  $request->input('position'),
                'price'         =>  $request->input('price'),
                'offer'         =>  $request->input('offer'),
                'new'           =>  $request->input('new'),
                'spicy'         =>  $request->input('spicy'),
                'special'       =>  $request->input('special'),
                'recommended'   =>  $request->input('recommended'),
                'group_id'      =>  $request->input('group_id'),
                'updated_at'    =>  date('Y-m-d H:i:s'),
                'created_at'    =>  date('Y-m-d H:i:s')
            ]);
            $json = [
                'type'     => 'success',
                'message'  => $item->english_name.' has been stored successfully.',
                'title'    => 'Success!'
            ];
            return Response::json($json);
        }
        return Response::json($validate->errors());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $item = Item::find($id);
        $item->category = $item->category;
        $item->likes;
        $item->favorites;
        return Response::json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $item = Item::find($id);

        $validate = Validator::make($request->all(), [
            'english_name-u'  =>  'required',
            'image'           =>  'image',
            'position-u'      =>  'numeric',
            'price-u'         =>  'numeric',
            'offer-u'         =>  'numeric',
        ]);

        $error = $validate->fails();
        if (!$error){

            $item->english_name     =  $request->input('english_name-u');
            $item->spanish_name     =  $request->input('spanish_name-u');
            $item->position         =  $request->input('position-u');
            $item->price            =  $request->input('price-u');
            $item->offer            =  $request->input('offer-u');
            $item->new              =  $request->input('new-u');
            $item->spicy            =  $request->input('spicy-u');
            $item->special          =  $request->input('special-u');
            $item->recommended      =  $request->input('recommended-u');

            $file = Input::file('image');

            if($file!=null){
                $path = public_path('storage/images/items');
                $image = Image::make(Input::file('image'));
                $image_name = str_replace(' ', '_', $request->input('english_name-u')).'.'.$file->getClientOriginalExtension();
                $height = $image->height();
                $width = $image->width();
                $image->backup();
                $image->save($path.'/'.$image_name);
                if($height < $width){ $image->heighten(80); }
                else{ $image->widen(80); }
                $image->crop(80,80);
                $image->save($path.'/avatar_'.$image_name);
                $image->reset();
                if($height < $width){ $image->heighten(130); }
                else{ $image->widen(130); }
                $image->crop(130,130);
                $image->save($path.'/preview_'.$image_name);
                $image->crop(300,150);
                $image->save($path.'/card_'.$image_name);
                $image->reset();
                if($height < $width){ $image->heighten(500); }
                else{ $image->widen(500); }
                $image->crop(500,500);
                $image->save($path.'/thumb_'.$image_name);

                $item->image = $image_name;
            }

            $item->description     =  $request->input('description-u');

            $item->updated_at =  date('Y-m-d H:i:s');
            $item->save();

            $json = [
                'type'     => 'success',
                'message'  => $item->english_name.' has been stored successfully.',
                'title'    => 'Success!'
            ];
            return Response::json($json);
        }
        return Response::json($validate->errors());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $item = Item::find($id);
        $name = $item->english_name;
        Item::destroy($id);
        $json = [
            'type' => 'success',
            'message' => $name.' has been removed successfully.',
            'title' => 'Success!'
        ];
        return Response::json($json);
    }
}
