<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use Validator;
use Response;
use Auth;

use Storage;
use File;
use Input;
use Image;

use App\User;

class UsersController extends Controller{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request){
        //date_default_timezone_set("America/Caracas");
        $this->middleware('auth');
        $this->middleware('ajax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $users = User::where('role', 'standard')->orderBy('created_at', 'desc')->get();
        foreach ($users as $user) { $user->fullname = $user->fullname; }
        $admin = User::where('role', 'administrator')->orderBy('created_at', 'desc')->get();
        foreach ($admin as $admi) { $admi->fullname = $admi->fullname; }
        $data = ['users' => $users, 'admin' => $admin];
        return Response::json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validate = Validator::make($request->all(), [
            'first_name'	=>  'required|alpha',
            'last_name'		=>  'required|alpha',
            'phone_number'	=>  'required',
            'email'       	=>  'required|email',
            'address'		=>  'required',
            'role'			=>  'required',
            'password'    	=>  'required|confirmed|min:8'
        ]);

        $error = $validate->fails();
        if (!$error){
            $user = User::create([
                'first_name'    =>  $request->input('first_name'),
                'last_name'     =>  $request->input('last_name'),
                'phone_number'  =>  $request->input('phone_number'),
                'email'         =>  $request->input('email'),
                'address'       =>  $request->input('address'),
                'password'      =>  Hash::make($request->input('password')),
                'role'          =>  $request->input('role'),
                'updated_at'    =>  date('Y-m-d H:i:s'),
                'created_at'    =>  date('Y-m-d H:i:s')
            ]);
            $json = [
                'type'     => 'success',
                'message'  => $user->fullname.' has been created successfully.',
                'title'    => 'Success!'
            ];
            return Response::json($json);
        }

        return Response::json($validate->errors());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $user = User::find($id);
        $user->fullname = $user->fullname;
        return Response::json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $user = User::find($id);
        $validate = Validator::make($request->all(), [
            'first_name-u'  =>  'required|alpha',
            'last_name-u'   =>  'required|alpha',
            'phone_number-u'  =>  'required',
            'email-u'         =>  'required|email',
            'address-u'       =>  'required',
        ]);
        $error = $validate->fails();
        if (!$error){
            $user->first_name    =  $request->input('first_name-u');
            $user->last_name     =  $request->input('last_name-u');
            $user->email         =  $request->input('email-u');
            $user->phone_number  =  $request->input('phone_number-u');
            $user->address       =  $request->input('address-u');
            $user->updated_at    =  date('Y-m-d H:i:s');
            $user->save();
            $json = [
                'type'     => 'success',
                'message'  => $user->fullname.' has been updated successfully.',
                'title'    => 'Success!'
            ];
            return Response::json($json);
        }
        return Response::json($validate->errors());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $user = User::find($id);
        $fullname = $user->fullname;
        User::destroy($id);
        $json = [
            'type' => 'success',
            'message' => $fullname.' has been removed successfully.',
            'title' => 'Success!'
        ];
        return Response::json($json);
    }










    /**
     * Change Image User.
     *
     * @return \Illuminate\Http\Response
     */
    public function image(Request $request, $id){
        $user = User::find($id);
        $file = Input::file('image');
        $path = public_path('storage/images/users/');
        $image = Image::make($file);
        $image->backup();
        $name = $id.'_image_'.date('YmdHis').'.png';
        $image->save($path.$name);
        File::delete($user->image);
        $user->image = 'storage/images/users/'.$name;
        $user->save();

        $json = [
            'type'     => 'success',
            'message'  => 'You picture has benn updated successfully.',
            'title'    => 'Success!',
            'image'    => $user->image
        ];

        return Response::json($json);
    }

    /**
     * Save Basic Information User.
     *
     * @return \Illuminate\Http\Response
     */
    public function basic(Request $request){
        $user = User::find($request->input('id'));
        $validate = Validator::make($request->all(),[
            'first_name'  => 'required|alpha|max:50',
            'last_name'   => 'required|alpha|max:50',
            'address'     => 'required'
        ]);

        $error = $validate->fails();
        if(!$error){
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->address = $request->input('address');
            $user->save();

            $json = [
                'type' => 'success',
                'message' => 'Your basic info has been updated seccessfully.',
                'title' => 'Success!'
            ];
        }
        else{
            $json = [
                'type' => 'danger',
                'message' => 'There was an error while trying to update the basic information.',
                'title' => 'Error!'
            ];
        }
        return Response::json($json);
    }

    /**
     * Change Password User.
     *
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request){
        $user = User::find($request->input('id'));
        $validate = Validator::make($request->all(),[
            'old_password'  => 'required|alpha_dash',
            'password'   => 'required|alpha_dash|min:6|confirmed',
        ]);
        $error = $validate->fails();
        if(!$error){
            if(Hash::check($request->input('old_password'), Auth::user()->password)){
                $user->password = bcrypt($request->input('password'));
                $user->save();
                $json = [
                    'type' => 'success',
                    'message' => 'Your password has been updated successfully.',
                    'title' => 'Hecho!'
                ];
            }
            else{
               $json = [
                    'type' => 'danger',
                    'message' => 'current password is invalid.',
                    'title' => 'Error!'
                ];
            }
        }
        else{
            $json = [
                'type' => 'danger',
                'message' => 'There was an error while trying to update the password.',
                'title' => 'Error!',
                'errors' => $validate->errors()
            ];
        }
        return Response::json($json);
    }

    /**
     * Save Contact Information User.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact(Request $request){
        $user = User::find($request->input('id'));
        $validate = Validator::make($request->all(),[
            'phone_number'  => 'required',
            'email'         => 'required|email',
        ]);

        $error = $validate->fails();
        if(!$error){
            $user->phone_number = $request->input('phone_number');
            $user->email = $request->input('email');
            $user->save();
            $json = [
                'type' => 'success',
                'message' => 'Your contact information has been updated successfully.',
                'title' => 'Hecho!'
            ];
        }
        else{
            $json = [
                'type' => 'danger',
                'message' => 'There was an error while trying to update the contact information.',
                'title' => 'Error!'
            ];
        }
        return Response::json($json);
    }
}

