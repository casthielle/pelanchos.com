<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use Validator;
use Response;
use Auth;

//use Storage;
//use File;
//use Input;
//use Image;

use App\Reservation;

class ReservationsController extends Controller{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //date_default_timezone_set("America/Caracas");
        $this->middleware('auth');
        $this->middleware('ajax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $reservations = Reservation::orderBy('updated_at', 'desc')->get();
        foreach ($reservations as $reservation) {
            $reservation->book_type = ucfirst($reservation->book_type);
            $reservation->date = Carbon::createFromFormat('Y-m-d',$reservation->date)->format('m/d/Y');
            $reservation->subsidiary_name = $reservation->subsidiary->name;
        }
        $data = $reservations;
        return Response::json($data);
    }

    /**
     * Display a count of resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function notifications(){
        $notifications = Reservation::where('status', 'on hold')->count();
        return Response::json($notifications);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validate = Validator::make($request->all(), [
            'name'          =>  'required',
            'email'         =>  'required|email',
            'phone_number'  =>  'required',
            'table_size'    =>  'required|integer',
            'date'          =>  'required|date_format:m/d/Y',
            'book_type'     =>  'required',
            'subsidiary_id' =>  'required'
        ]);

        $error = $validate->fails();
        if (!$error){
            $date = Carbon::createFromFormat('m/d/Y', $request->input('date'));
            $reservation = Reservation::create([
                'name'          =>  $request->input('name'),
                'email'         =>  $request->input('email'),
                'phone_number'  =>  $request->input('phone_number'),
                'table_size'    =>  $request->input('table_size'),
                'date'          =>  $date,
                'book_type'     =>  $request->input('book_type'),
                'subsidiary_id' =>  $request->input('subsidiary_id'),
                'status'        =>  'approved',
                'requirements'  =>  $request->input('requirements'),
                'updated_at'    =>  date('Y-m-d H:i:s'),
                'created_at'    =>  date('Y-m-d H:i:s')
            ]);
            $json = [
                'type'     => 'success',
                'message'  => 'The table has been booked successfully.',
                'title'    => 'Success!'
            ];
            return Response::json($json);
        }

        return Response::json($validate->errors());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $reservation = Reservation::find($id);
        $reservation->date = Carbon::createFromFormat('Y-m-d', $reservation->date)->format('m/d/Y');
        return Response::json($reservation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $reservation = Reservation::find($id);
        if($request->has('change')){
            $validate = Validator::make($request->all(), [
                'status-u'          =>  'required'
            ]);
            $error = $validate->fails();
            if (!$error){
                $reservation->status  =  $request->input('status-u');
                $reservation->updated_at    =  date('Y-m-d H:i:s');
                $reservation->save();
                $json = [
                    'type'     => 'success',
                    'message'  => 'The booking has been updated successfully.',
                    'title'    => 'Success!'
                ];
                return Response::json($json);
            }
            return Response::json($validate->errors());
        }
        else{
            $validate = Validator::make($request->all(), [
                'name-u'          =>  'required',
                'email-u'         =>  'required|email',
                'phone_number-u'  =>  'required',
                'table_size-u'    =>  'required|integer',
                'date-u'          =>  'required|date_format:m/d/Y',
                'book_type-u'     =>  'required',
                'subsidiary_id-u' =>  'required'
            ]);
            $error = $validate->fails();
            if (!$error){
                $date = Carbon::createFromFormat('m/d/Y', $request->input('date-u'));
                $reservation->name          =  $request->input('name-u');
                $reservation->email         =  $request->input('email-u');
                $reservation->phone_number  =  $request->input('phone_number-u');
                $reservation->table_size    =  $request->input('table_size-u');
                $reservation->date          =  $date;
                $reservation->book_type     =  $request->input('book_type-u');
                $reservation->subsidiary_id =  $request->input('subsidiary_id-u');
                $reservation->requirements  =  $request->input('requirements-u');
                $reservation->updated_at    =  date('Y-m-d H:i:s');
                $reservation->save();
                $json = [
                    'type'     => 'success',
                    'message'  => 'The booking has been updated successfully.',
                    'title'    => 'Success!'
                ];
                return Response::json($json);
            }
            return Response::json($validate->errors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $reservation = Reservation::find($id);
        Reservation::destroy($id);
        $json = [
            'type' => 'success',
            'message' => 'The booking has been removed successfully.',
            'title' => 'Success!'
        ];
        return Response::json($json);
    }
}
