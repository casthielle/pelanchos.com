<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use Validator;
use Response;
use Auth;

use Storage;
use File;
use Input;
use Image;

use App\Event;

class EventsController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request){
        //date_default_timezone_set("America/Caracas");
        $this->middleware('auth');
        $this->middleware('ajax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $events = Event::orderBy('date', 'desc')->get();
        foreach ($events as $event){
            $event->date = Carbon::createFromFormat('Y-m-d',$event->date)->format('m/d/Y');
            $event->subsidiary = $event->subsidiary->name;
        }
        return Response::json($events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validate = Validator::make($request->all(), [
            'name'          =>  'required',
            'date'          =>  'required|date_format:m/d/Y',
            'image'         =>  'image',
            'subsidiary_id' =>  'required|integer'
        ]);

        $error = $validate->fails();
        if (!$error){

            $date = Carbon::createFromFormat('m/d/Y', $request->input('date'));

            $file = Input::file('image');
            $path = public_path('storage/images/events');
            $image = Image::make(Input::file('image'));

            $image_name = str_replace(' ', '_', $request->input('name')).'.'.$file->getClientOriginalExtension();
            $height = $image->height();
            $width = $image->width();
            $image->backup();
            $image->save($path.'/'.$image_name);
            if($height < $width){ $image->heighten(80); }
            else{ $image->widen(80); }
            $image->crop(80,80);
            $image->save($path.'/avatar_'.$image_name);
            $image->reset();
            if($height < $width){ $image->heighten(300); }
            else{ $image->widen(300); }
            $image->crop(300,300);
            $image->save($path.'/preview_'.$image_name);
            $image->crop(300,150);
            $image->save($path.'/card_'.$image_name);
            $image->reset();
            if($height < $width){ $image->heighten(500); }
            else{ $image->widen(500); }
            $image->crop(500,500);
            $image->save($path.'/thumb_'.$image_name);

            $event = Event::create([
                'name'          =>  $request->input('name'),
                'date'          =>  $date,
                'details'       =>  $request->input('details'),
                'image'         =>  $image_name,
                'subsidiary_id' =>  $request->input('subsidiary_id'),
                'updated_at'    =>  date('Y-m-d H:i:s'),
                'created_at'    =>  date('Y-m-d H:i:s')
            ]);
            $json = [
                'type'     => 'success',
                'message'  => $event->name.' has been stored successfully.',
                'title'    => 'Success!'
            ];
            return Response::json($json);
        }
        return Response::json($validate->errors());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $event = Event::find($id);
        $event->subsidiary;
        $event->date = Carbon::createFromFormat('Y-m-d',$event->date)->format('m/d/Y');
        return Response::json($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $event = Event::find($id);

        $validate = Validator::make($request->all(), [
            'name-u'          =>  'required',
            'date-u'          =>  'required|date_format:m/d/Y',
            'image'         =>  'image',
            'subsidiary_id-u' =>  'required|integer'
        ]);

        $error = $validate->fails();
        if (!$error){

            $date = Carbon::createFromFormat('m/d/Y', $request->input('date-u'));
            $file = Input::file('image');

            $event->name     =  $request->input('name-u');
            $event->date     =  $date;


            if($file!=null){
                $path = public_path('storage/images/events');
                $image = Image::make(Input::file('image'));
                $image_name = str_replace(' ', '_', $request->input('name-u')).'.'.$file->getClientOriginalExtension();
                $height = $image->height();
                $width = $image->width();
                $image->backup();
                $image->save($path.'/'.$image_name);
                if($height < $width){ $image->heighten(80); }
                else{ $image->widen(80); }
                $image->crop(80,80);
                $image->save($path.'/avatar_'.$image_name);
                $image->reset();
                if($height < $width){ $image->heighten(300); }
                else{ $image->widen(300); }
                $image->crop(300,300);
                $image->save($path.'/preview_'.$image_name);
                $image->crop(300,150);
                $image->save($path.'/card_'.$image_name);
                $image->reset();
                if($height < $width){ $image->heighten(500); }
                else{ $image->widen(500); }
                $image->crop(500,500);
                $image->save($path.'/thumb_'.$image_name);
                $event->image = $image_name;
            }

            $event->subsidiary_id   =  $request->input('subsidiary_id-u');
            $event->details         =  $request->input('details-u');

            $event->updated_at =  date('Y-m-d H:i:s');
            $event->save();

            $json = [
                'type'     => 'success',
                'message'  => $event->name.' has been stored successfully.',
                'title'    => 'Success!'
            ];

            return Response::json($json);
        }
        return Response::json($validate->errors());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $event = Event::find($id);
        $name = $event->name;
        Event::destroy($id);
        $json = [
            'type' => 'success',
            'message' => $name.' has been removed successfully.',
            'title' => 'Success!'
        ];
        return Response::json($json);
    }
}
