<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use Validator;
use Response;
use Auth;

use Storage;
use File;
use Input;
use Image;

use App\MenuGroup as Category;

class CategoriesController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request){
        //date_default_timezone_set("America/Caracas");
        $this->middleware('auth');
        $this->middleware('ajax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $categories = Category::orderBy('updated_at', 'desc')->get();
        return Response::json($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validate = Validator::make($request->all(), [
            'english_name'  =>  'required',
            'image'         =>  'required|image'
        ]);

        $error = $validate->fails();
        if (!$error){

            $file = Input::file('image');
            $path = public_path('storage/images/categories');
            $image = Image::make(Input::file('image'));

            $image_name = str_replace(' ', '_', $request->input('english_name')).'.'.$file->getClientOriginalExtension();
            $height = $image->height();
            $width = $image->width();
            $image->backup();
            $image->save($path.'/'.$image_name);
            if($height < $width){ $image->heighten(80); }
            else{ $image->widen(80); }
            $image->crop(80,80);
            $image->save($path.'/avatar_'.$image_name);
            $image->reset();
            if($height < $width){ $image->heighten(300); }
            else{ $image->widen(300); }
            $image->crop(300,300);
            $image->save($path.'/preview_'.$image_name);
            $image->crop(300,150);
            $image->save($path.'/card_'.$image_name);
            $image->reset();
            if($height < $width){ $image->heighten(500); }
            else{ $image->widen(500); }
            $image->crop(500,500);
            $image->save($path.'/thumb_'.$image_name);

            $category = Category::create([
                'english_name'  =>  $request->input('english_name'),
                'spanish_name'  =>  $request->input('spanish_name'),
                'image'         =>  $image_name,
                'description'   =>  $request->input('description'),
                'updated_at'    =>  date('Y-m-d H:i:s'),
                'created_at'    =>  date('Y-m-d H:i:s')
            ]);
            $json = [
                'type'     => 'success',
                'message'  => $category->english_name.' has been stored successfully.',
                'title'    => 'Success!'
            ];
            return Response::json($json);
        }
        return Response::json($validate->errors());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $category = Category::find($id);
        $category->elements;
        return Response::json($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $category = Category::find($id);

        $validate = Validator::make($request->all(), [
            'english_name-u'  =>  'required',
            'image'           =>  'image'
        ]);

        $error = $validate->fails();
        if (!$error){

            $category->english_name     =  $request->input('english_name-u');
            $category->spanish_name     =  $request->input('spanish_name-u');
            $file = Input::file('image');

            if($file!=null){
                $path = public_path('storage/images/categories');
                $image = Image::make(Input::file('image'));
                $image_name = str_replace(' ', '_', $request->input('english_name-u')).'.'.$file->getClientOriginalExtension();
                $height = $image->height();
                $width = $image->width();
                $image->backup();
                $image->save($path.'/'.$image_name);
                if($height < $width){ $image->heighten(80); }
                else{ $image->widen(80); }
                $image->crop(80,80);
                $image->save($path.'/avatar_'.$image_name);
                $image->reset();
                if($height < $width){ $image->heighten(300); }
                else{ $image->widen(300); }
                $image->crop(300,300);
                $image->save($path.'/preview_'.$image_name);
                $image->crop(300,150);
                $image->save($path.'/card_'.$image_name);
                $image->reset();
                if($height < $width){ $image->heighten(500); }
                else{ $image->widen(500); }
                $image->crop(500,500);
                $image->save($path.'/thumb_'.$image_name);

                $category->image = $image_name;
            }

            $category->description     =  $request->input('description-u');

            $category->updated_at =  date('Y-m-d H:i:s');
            $category->save();

            $json = [
                'type'     => 'success',
                'message'  => $category->english_name.' has been stored successfully.',
                'title'    => 'Success!'
            ];
            return Response::json($json);
        }
        return Response::json($validate->errors());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $category = Category::find($id);
        $name = $category->english_name;
        Category::destroy($id);
        $json = [
            'type' => 'success',
            'message' => $name.' has been removed successfully.',
            'title' => 'Success!'
        ];
        return Response::json($json);
    }
}
