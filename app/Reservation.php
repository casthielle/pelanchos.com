<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'email', 'address', 'phone_number', 'status', 'date', 'book_type', 'table_size', 'requirements', 'subsidiary_id' ];

    /**
     * Reservation belongs to Subsidiary.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subsidiary(){

        // belongsTo(RelatedModel, foreignKey = subsidiary_id, keyOnRelatedModel = id)
        return $this->belongsTo(Subsidiary::class);
    }
}
