<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'english_name',
        'spanish_name',
        'image',
        'description',
        'status',
        'position',
        'group_id',
        'price',
        'offer',
        'new',
        'spicy',
        'special',
        'recommended'
    ];

    /**
     * MenuItem belongs to Category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(){

    	// belongsTo(RelatedModel, foreignKey = category_id, keyOnRelatedModel = id)
    	return $this->belongsTo(MenuGroup::class, 'group_id');
    }

    /**
     * MenuItem has many Favorites.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favorites(){

        // hasMany(RelatedModel, foreignKeyOnRelatedModel = menuItem_id, localKey = id)
        return $this->hasMany(Favorite::class, 'item_id');
    }

    /**
     * MenuItem has many Likes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes(){

        // hasMany(RelatedModel, foreignKeyOnRelatedModel = menuItem_id, localKey = id)
        return $this->hasMany(Like::class, 'item_id');
    }
}
