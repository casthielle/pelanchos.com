<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuGroup extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'english_name', 'spanish_name', 'image', 'description', 'status', 'position' ];

	/**
	 * MenuGroup has many .
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function elements(){
		// hasMany(RelatedModel, foreignKeyOnRelatedModel = menuGroup_id, localKey = id)
		return $this->hasMany(MenuItem::class, 'group_id');
	}

}
