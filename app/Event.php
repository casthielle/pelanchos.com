<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'date', 'details', 'likes', 'image', 'subsidiary_id' ];

    /**
     * Event belongs to Subsidiary.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subsidiary(){

    	// belongsTo(RelatedModel, foreignKey = subsidiary_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Subsidiary::class);
    }

}
