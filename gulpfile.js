var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

	// Landing Page Styles and Scripts
	mix.styles([
		'animation.css',
		'bootstrap.min.css',
		'bxslider.css',
		'date-picker.css',
		'font-awesome.min.css',
		'sweet-alert.css',
		'magnific-popup.css',
		'main.css',
		'responsive.css',
		'style.css',
		'vegas.min.css'
	], 'public/css/index.css');
	mix.scripts([
		'jquery.js',
		'bootstrap.min.js',
		'bootstrap-datepicker.js',
		'gmaps.js',
		'jquery.parallax.js',
		'sweet-alert.min.js',
		'jquery.magnific-popup.min.js',
		'vegas.min.js',
		'jquery.bxslider.min.js',
		'main.js'
	], 'public/js/index.js');


	// Login Styles and Scripts
	mix.styles([
		'animate.min.css',
		'material-design-iconic-font.min.css',
		'app.min.1.css',
		'app.min.2.css',
		'style.css'
	], 'public/css/login.css');
	mix.scripts([
		'jquery.min.js',
		'bootstrap-admin.min.js',
		'waves.min.js',
		'functions.js'
	], 'public/js/login.js');


	// Admin Styles and Scripts
	mix.styles([
		'fullcalendar.min.css',
		'animate.min.css',
		'sweet-alert.css',
		'material-design-iconic-font.min.css',
		'jquery.mCustomScrollbar.min.css',
		'bootstrap-datetimepicker.min.css',
		'bootstrap-select.css',
		'mediaelementplayer.css',
		'app.min.1.css',
		'app.min.2.css',
		'style.css'
	], 'public/css/admin.css');
	mix.scripts([
		'jquery.min.js',
		'autosize.min.js',
		'moment.min.js',
		'bootstrap-admin.min.js',
		'bootstrap-datetimepicker.min.js',
		'bootstrap-select.js',
		'jquery.flot.js',
		'jquery.flot.resize.js',
		'curvedLines.js',
		'mediaelement-and-player.min.js',
		'jquery.sparkline.min.js',
		'jquery.easypiechart.min.js',
		'fullcalendar.min.js',
		'fileinput.min.js',
		'jquery.simpleWeather.min.js',
		'jquery.bootgrid.updated.min.js',
		'waves.min.js',
		'bootstrap-growl.min.js',
		'sweet-alert.min.js',
		'input-mask.min.js',
		'jquery.mCustomScrollbar.concat.min.js',
		'curved-line-chart.js',
		'line-chart.js',
		'charts.js',
		'functions.js',
		'demo.js',
		'jquery.tmpl.min.js'
	], 'public/js/admin.js');
});
