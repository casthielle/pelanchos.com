<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'status' => 'active',
        'role' => 'standard',
        'email' => $faker->safeEmail,
        'address' => $faker->address,
        'phone_number' => $faker->tollFreePhoneNumber,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Subsidiary::class, function (Faker\Generator $faker) {
    return [
        'name'          => $faker->company,
        'email'         => $faker->safeEmail,
        'location'      => $faker->address,
        'phone_number'  => $faker->tollFreePhoneNumber,
    ];
});


$factory->define(App\Reservation::class, function (Faker\Generator $faker) {
    return [
        'name'          =>  $faker->name,
        'email'         =>  $faker->safeEmail,
        'phone_number'  =>  $faker->tollFreePhoneNumber,
        'table_size'    =>  $faker->randomDigitNotNull,
        'date'          =>  $faker->dateTimeBetween('now', '2 weeks')->format('Y-m-d'),
        'book_type'     =>  $faker->randomElement(['breakfast', 'lunch', 'dinner']),
        'requirements'  =>  $faker->sentence(6,true),
        'status'        =>  $faker->randomElement(['on hold', 'approved', 'rejected', 'confirmed', 'cancelled', 'in progress', 'finalized']),
        'subsidiary_id' =>  $faker->randomElement([1,2])
    ];
});


$factory->define(App\Company::class, function (Faker\Generator $faker) {
    return [
        'name'          =>  $faker->name,
        'image'         =>  $faker->image(null, '400', '400', null, false),
        'email'         =>  $faker->safeEmail,
        'phone_number'  =>  $faker->tollFreePhoneNumber,
        'address'       =>  $faker->address,
        'web'           =>  $faker->domainName
    ];
});


