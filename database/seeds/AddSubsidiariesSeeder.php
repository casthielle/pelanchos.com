<?php

use Illuminate\Database\Seeder;

class AddSubsidiariesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        factory(App\Subsidiary::class)->create([
            'name'			=> 'Knoxville',
            'email'			=> 'example@example.com',
            'location'		=> '1516 Downtown West Blvd. Knoxville, TN 37919.',
            'phone_number'  => '+1 (865) 694 90 60',
            'company_id'    => '1'
        ]);

        factory(App\Subsidiary::class)->create([
            'name'			=> 'Seymour',
            'email'			=> 'example@example.com',
            'location'		=> '10721 Chapman Highway. Seymour, TN 37865. Macon Crossing Center.',
            'phone_number'  => '+1 (865) 773 04 75',
            'company_id'    => '1'
        ]);

    }
}
