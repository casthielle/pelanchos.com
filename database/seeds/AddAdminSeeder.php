<?php

use Illuminate\Database\Seeder;

class AddAdminSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        factory(App\User::class, 9)->create([
            'role' => 'administrator'
        ]);

        factory(App\User::class)->create([
            'first_name'    => 'Joel',
            'last_name'     => 'Crespo',
            'role'          => 'root',
            'phone_number'  => '(1233) 312 1231',
            'address'       => 'Calle la loquera verde frente a tu mamab y tu papapa que seas serio.',
            'email'         => 'casthielle@gmail.com',
            'password'      => Hash::make('Mqxe23ow456.'),
            'created_at'    => date('y-m-d H:i:s'),
            'updated_at'    => date('y-m-d H:i:s')
        ]);

    }
}
