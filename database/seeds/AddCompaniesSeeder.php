<?php

use Illuminate\Database\Seeder;

class AddCompaniesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        factory(App\Company::class)->create([
            'name'          =>  'Pelanchos',
	        'image'         =>  'logo.svg',
	        'email'         =>  'info@pelanchos.com',
	        'phone_number'  =>  '+18656949060',
	        'address'       => 	'1516 Downtown West Blvd. Knoxville, TN 37919',
	        'web'			=>	'pelanchos.com'
        ]);
    }
}
