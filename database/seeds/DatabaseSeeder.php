<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $this->call(AddAdminSeeder::class);
        $this->call(AddUsersSeeder::class);
        $this->call(AddCompaniesSeeder::class);
        $this->call(AddSubsidiariesSeeder::class);
        $this->call(AddReservationsSeeder::class);

    }
}
