<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuSpecialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_specials', function (Blueprint $table) {
            $table->increments('id');
            $table->text('details')->nullable();
            $table->text('comments')->nullable();
            $table->date('date');

            $table->integer('item_id')->unsigned();
            $table->integer('subsidiary_id')->unsigned();

            $table->nullableTimestamps();

            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_specials');
    }
}
