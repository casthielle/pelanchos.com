<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyConstraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('menu_items', function($table) {
            $table->foreign('group_id')->references('id')->on('menu_groups')->onDelete('cascade');
        });

        Schema::table('favorites', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('menu_items')->onDelete('cascade');
        });

        Schema::table('likes', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('menu_items')->onDelete('cascade');
        });

        Schema::table('tables', function($table) {
            $table->foreign('subsidiary_id')->references('id')->on('subsidiaries')->onDelete('cascade');
        });

        Schema::table('menu_specials', function($table) {
            $table->foreign('item_id')->references('id')->on('menu_items')->onDelete('cascade');
            $table->foreign('subsidiary_id')->references('id')->on('subsidiaries')->onDelete('cascade');
        });

        Schema::table('reservations', function($table) {
            $table->foreign('subsidiary_id')->references('id')->on('subsidiaries')->onDelete('cascade');
        });

        Schema::table('events', function($table) {
            $table->foreign('subsidiary_id')->references('id')->on('subsidiaries')->onDelete('cascade');
        });

        Schema::table('subsidiaries', function($table) {
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
