<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone_number');
            $table->integer('table_size');
            $table->date('date');
            $table->enum('book_type', ['breakfast', 'lunch', 'dinner']);
            $table->text('requirements')->nullable();
            $table->enum('status', ['on hold', 'approved', 'rejected', 'confirmed', 'cancelled', 'in progress', 'finalized']);

            $table->integer('subsidiary_id')->unsigned();

            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::drop('reservations');
    }
}
