<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('english_name');
            $table->string('spanish_name')->nullable();
            $table->text('description')->nullable();
            $table->text('image')->nullable();
            $table->integer('position')->nullable();
            $table->integer('price')->nullable();
            $table->integer('offer')->nullable();

            $table->integer('group_id')->unsigned();

            $table->boolean('new')->nullable(); // green
            $table->boolean('spicy')->nullable(); // red
            $table->boolean('special')->nullable(); // amber
            $table->boolean('recommended')->nullable(); // blue
            $table->boolean('status')->nullable();

            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_items');
    }
}
