<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Material Admin</title>

        <!-- Vendor CSS -->
        {!! Html::style('css/login.css') !!}

    </head>

    <body class="login-content">

        <!-- Login -->
        <h2 class="name-logo">Pelanchos</h2>
        <div class="lc-block toggled" id="l-login">
            {!! Form::open(['url' => '/login']) !!}

                <div class="input-group m-b-20 {{ $errors->has('email') ? 'has-error' : '' }}">
                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                    <div class="fg-line">
                        <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="input-group m-b-20 {{ $errors->has('password') ? 'has-error' : '' }}">
                    <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                    <div class="fg-line">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember">
                        <i class="input-helper"></i>
                        Keep me signed in
                    </label>
                </div>

                <button type="submit" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>

                <ul class="login-navigation">
                    <li data-block="#l-forget-password" class="bgm-orange">Forgot Password?</li>
                </ul>
            {!! Form::close() !!}
        </div>

        <!-- Forgot Password -->
        <div class="lc-block" id="l-forget-password">
            {!! Form::open(['url' => '/password/email']) !!}
            <div class="input-group m-b-20 {{ $errors->has('email') ? 'has-error' : '' }}">
                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                <div class="fg-line">
                    <input type="email" name="email" class="form-control" placeholder="Email">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <button type="submit" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>

            <ul class="login-navigation">
                <li data-block="#l-login" class="bgm-red">Login</li>
            </ul>
            {!! Form::close() !!}
        </div>

        <!-- Javascript Libraries -->
        {!! Html::script('js/login.js') !!}

    </body>
</html>