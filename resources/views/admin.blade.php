@extends('base.base')


@section('content')

    @include('views.home')
    @include('views.users')
    @include('views.reservations')
    @include('views.menu')
    @include('views.items')
    @include('views.events')
    @include('views.startup')
    @include('views.settings')

    @include('base.modals')

@endsection


@section('scripts')

    @include('scripts.javascript')
    @include('scripts.templates')

@endsection