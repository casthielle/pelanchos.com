<script type="text/x-template" id="template">

	<a href="#" data-view='users' data-table='users' data-color='blue' class="lv-item" id="${id}" data-row-id="${id}" onclick='$.details(event, $(this))' data-modal='modal-view-users'>
	    <div class="media">
	        <div class="pull-left">
	            <img class="lv-img-sm" src="${image}" alt="">
	        </div>
	        <div class="media-body">
	            <div class="lv-title">${name}</div>
	            <small class="lv-small">
	                Email: ${email} - Phone Number: ${phone_number}
	            </small>
	        </div>
	    </div>
	</a>

</script>


<script type="text/x-template" id="tr-template">

	<tr>
		<td>${id}</td>
		<td>${name}</td>
		<td>${description}</td>
		<td>
			<button data-color='lightblue' data-table="categories" class='card-list btn btn-icon btn-default waves-effect waves-circle waves-float' 	data-form='form-edit-categories' data-row-id='${id}' onclick='$.edit(event, $(this))' data-modal='modal-edit-categories'><span class='zmdi zmdi-edit'></span></button>
			<button data-color='lightblue' data-table="categories" class='card-list btn btn-icon btn-default waves-effect waves-circle waves-float' 	data-row-id='${id}' onclick='$.elements(event, $(this))' data-view="items" id="category-${id}"><span class='zmdi zmdi-eye'></span></button>
			<button data-color='lightblue' data-table="categories" class='card-list btn btn-icon bgm-red waves-effect waves-circle waves-float' 		data-form='form-delete-categories' data-row-id='${id}' onclick='$.delete(event, $(this))'><span class='zmdi zmdi-delete'></span></button>
		</td>
	</tr>

</script>



<script type="text/x-template" id="tr-template-item">

	<tr>
		<td>${id}</td>
		<td>${name}</td>
		<td>${price}</td>
		<td>
			<span class='c-green f-10'>${nw}</span>
			<span class='c-red f-10'>${spicy}</span>
			<span class='c-amber f-10'>${special}</span>
			<span class='c-blue f-10'>${recommended}</span>
		</td>
		<td>
			<button data-color='lightblue' data-table="items" class='card-list btn btn-icon btn-default waves-effect waves-circle waves-float' 	data-form='form-edit-items' data-row-id='${id}' onclick='$.edit(event, $(this))' data-modal='modal-edit-items'><span class='zmdi zmdi-edit'></span></button>
			<button data-color='red' data-table="items" class='card-list btn btn-icon btn-default waves-effect waves-circle waves-float' 	data-row-id='${id}' onclick='$.details(event, $(this))' data-modal='modal-view-items'><span class='zmdi zmdi-eye'></span></button>
			<button data-color='lightblue' data-table="items" class='card-list btn btn-icon bgm-red waves-effect waves-circle waves-float' 		data-form='form-delete-items' data-row-id='${id}' onclick='$.delete(event, $(this))'><span class='zmdi zmdi-delete'></span></button>
		</td>
	</tr>

</script>


<script type="text/x-template" id="tr-template-event">

	<tr>
		<td>${id}</td>
		<td>${name}</td>
		<td>${date}</td>
		<td>${subsidiary}</td>
		<td>
			<button data-color='amber' 		data-table="events" class='card-list btn btn-icon btn-default waves-effect waves-circle waves-float' 	data-form='form-edit-events' data-row-id='${id}' onclick='$.edit(event, $(this))' data-modal='modal-edit-events'><span class='zmdi zmdi-edit'></span></button>
			<button data-color='red' 		data-table="events" class='card-list btn btn-icon btn-default waves-effect waves-circle waves-float' 	data-row-id='${id}' onclick='$.details(event, $(this))' data-modal='modal-view-items'><span class='zmdi zmdi-eye'></span></button>
			<button data-color='lightblue' 	data-table="events" class='card-list btn btn-icon bgm-red waves-effect waves-circle waves-float' 		data-form='form-delete-events' data-row-id='${id}' onclick='$.delete(event, $(this))'><span class='zmdi zmdi-delete'></span></button>
		</td>
	</tr>

</script>