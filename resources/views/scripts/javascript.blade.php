<script>
    update = true;
    view = "";
    current_object = "";
    notification = 0;
    startup = "";

	$(window).on('load', function(){
        $('.views').css('display','none');
        $('#home').css('display','block');
        $('.view').click(function(e){
            e.preventDefault();
            $('.nav-btn').removeClass('active');
            var btn = $(this).parents('li').addClass('active');
            var view = btn.data('view');
            $('.views').css('display','none');
            $('#'+view).css('display','block');
            window.view = view;
        });
    });

    $(document).on('ready', function(){

        $('.custom-checkbox').change(function(){
            if($(this).is(':checked')){ $(this).val(1); }
            else{ $(this).val(0); }
        });

        $('.btn-save-file').hover(                  function(e){
            window.update = false; }, function(e){
            window.update = true;
        });

        $('.modal').on('shown.bs.modal',            function(e){
            //
          window.update = false;
        });

        $('.modal').on('hidden.bs.modal',           function(e){
            //
          window.update = true;
        });

        $('body').on('click', '.btn-with-color',    function(e){
            var color = $(this).data('target-color');
            $($(this).attr('href')).attr('data-modal-color', color);
        });

        $('body').on('mouseover', '.card-list',     function(e){
            //
            window.update = false;
        });

        $('body').on('mouseout', '.card-list',      function(e){
            //
            window.update = true;
        });

        $('body').on('click', '.btn-save',          function(e){
            e.preventDefault();
            var btn = $(this);
            var form  =  $('#'+btn.data('form'));
            var url   =  form.attr('action');
            var data  =  form.serialize();
            $.post(url, data, function(response){
                $('div.form-group').removeClass('has-error');
                $('.help-block').html("");
                if(response.type){
                    swal(response.title, response.message, response.type);
                    form.each(function(){
                        this.reset();
                    });
                    $('.selectpicker').selectpicker('val', null);
                    btn.parents('div.modal').modal('hide');
                }
                else{
                    for (index in response){
                        $('#'+btn.data('form')+' input[name='+index+']').parents('div.form-group').addClass('has-error');
                        $('#'+btn.data('form')+' small#error-'+index).html(response[index]);
                    }
                }
            });
        });

        $('body').on('click', '.btn-save-file',     function(e){
            e.preventDefault();
            var btn = $(this);
            var form  =  $('#'+btn.data('form'));
            var url   =  form.attr('action');
            //var data  =  form.serialize();
            var data = new FormData(document.getElementById(btn.data('form')));

            $.ajaxSetup({ cache: false, contentType: false, processData: false });

            $.post(url, data, function(response){
                $('div.form-group').removeClass('has-error');
                $('.help-block').html("");
                if(response.type){
                    swal(response.title, response.message, response.type);
                    form.each(function(){
                        this.reset();
                    });
                    $('.fileinput').fileinput('clear');
                    $('.selectpicker').selectpicker('val', null);
                    if(btn.data('form')=='form-add-items'){
                        $.ajaxSetup({ cache: true, contentType: 'application/x-www-form-urlencoded; charset=UTF-8', processData: true });
                        $("#category-"+$(".group-id").val()).click();
                    };
                    btn.parents('div.modal').modal('hide');
                }
                else{
                    for(index in response){
                        $('#'+btn.data('form')+' input[name='+index+']').parents('div.form-group').addClass('has-error');
                        $('#'+btn.data('form')+' small#error-'+index).html(response[index]);
                    }
                };
            });
        });

        $('body').on('click', '.breadcrumb-back',   function(e){
            $('.views').css('display','none');
            $('#'+$(this).data('back')).css('display','block');
        });

        $('body').on('click', '.btn-update',        function(e){
            e.preventDefault();
            var btn = $(this);
            var form  =  $('#'+btn.data('form'));
            var url   =  form.attr('action').replace(':UI', btn.data('id'));
            var data  =  form.serialize();
            $.post(url, data, function(response){
                $('div.form-group').removeClass('has-error');
                $('.help-block').html("");
                if(response.type){
                    swal(response.title, response.message, response.type);
                    form.each(function(){
                        this.reset();
                    });
                    btn.parents('div.modal').modal('hide');
                }
                else{
                    for (index in response){
                        $('#'+btn.data('form')+' input[name='+index+']').parents('div.form-group').addClass('has-error');
                        $('#'+btn.data('form')+' small#error-'+index).html(response[index]);
                    }
                }
                $.update(window.view);
            });
        });

        $('body').on('click', '.btn-update-file',   function(e){
            e.preventDefault();
            var btn = $(this);
            var form  =  $('#'+btn.data('form'));
            var url   =  form.attr('action').replace(':UI', btn.data('id'));
            var data = new FormData(document.getElementById(btn.data('form')));

            $.ajaxSetup({ cache: false, contentType: false, processData: false });

            $.post(url, data, function(response){
                $('div.form-group').removeClass('has-error');
                $('.help-block').html("");
                if(response.type){
                    swal(response.title, response.message, response.type);
                    form.each(function(){
                        this.reset();
                    });
                    $('.fileinput').fileinput('clear');
                    $('.selectpicker').selectpicker('val', null);
                    if(btn.data('form')=='form-edit-items'){
                        $.ajaxSetup({ cache: true, contentType: 'application/x-www-form-urlencoded; charset=UTF-8', processData: true });
                        $("#category-"+$(".group-id").val()).click();
                    }
                    else{
                        $.update(window.view);
                    };
                    btn.parents('div.modal').modal('hide');
                }
                else{
                    for(index in response){
                        $('#'+btn.data('form')+' input[name='+index+']').parents('div.form-group').addClass('has-error');
                        $('#'+btn.data('form')+' small#error-'+index).html(response[index]);
                    }
                };
            });
        });

        $('body').on('click', '.btn-cancel',        function(e){
            $('div.form-group').removeClass('has-error');
            $('.help-block').html("");
        });

        $('body').on('click', '.btn-save-basic',    function(e){
            e.preventDefault();
            var btn = $(this);
            var form  =  btn.parents('form');
            var url   =  form.attr('action');
            var data  =  form.serialize();
            $.post(url, data, function(response){
                $.message(response.title, response.message, response.type);
                if(response.type=="success"){
                    $("#reset-basic").click();
                    $.user();
                }
            });
        });

        $('body').on('click', '.btn-save-contact',  function(e){
            e.preventDefault();
            var btn = $(this);
            var form  =  btn.parents('form');
            var url   =  form.attr('action');
            var data  =  form.serialize();
            $.post(url, data, function(response){
                $.message(response.title, response.message, response.type);
                if(response.type=="success"){
                    $("#reset-contact").click();
                    $.user();
                }
            });
        });

        $('body').on('click', '.btn-image',         function(e){
            var btn = $(this);
            e.preventDefault();
            var url = $('#'+$(this).data('form')).attr('action').replace(':UI', "{{ Auth::user()->id }}" );
            var data = new FormData(document.getElementById($(this).data('form')));
            $.ajaxSetup({
                cache: false,
                contentType: false,
                processData: false
            });
            $.post(url, data, function(response){
                $.ajaxSetup({
                    cache: true,
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    processData: true
                });
                if(response.type=='success'){
                    $.message(response.title, response.message, response.type);
                    btn.parents('div.modal').modal('hide');
                    $('.img-profile').attr('src', response.image);
                }
            });
        });

        $.datatable =   function(datatable, table, options={}){
            $("#datatable-"+datatable).bootgrid({
                caseSensitive: false,
                rowCount: 4,
                columnSelection: options.colSelection,
                css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less',
                },
                formatters: {
                    "commands": function(column, row){
                        var btn_view = "";
                        if(table=="users"){
                            var btn_view = "<button data-color='blue' data-view='users' class='btn btn-icon btn-default waves-effect waves-circle' data-datatable='" + datatable + "' data-table='" + table + "' data-row-id='" + row.id + "' onclick='$.details(event, $(this))' data-modal='modal-view-users'><span class='zmdi zmdi-eye'></span></button>";
                        };

                        return "<button data-color='red' class='btn btn-icon btn-default waves-effect waves-circle' data-datatable='" + datatable + "' data-table='" + table + "' data-form='form-edit-" + table + "'  data-row-id='" + row.id + "' onclick='$.edit(event, $(this))' data-modal='modal-edit-" + table + "'><span class='zmdi zmdi-edit'></span></button> " +
                               btn_view + "<button data-color='red' class='btn btn-icon btn-default waves-effect waves-circle' data-datatable='" + datatable + "' data-table='" + table + "' data-form='form-delete-" + table + "' data-row-id='" + row.id + "' onclick='$.delete(event, $(this))'><span class='zmdi zmdi-delete'></span></button>";
                    },

                    "status": function(column, row){
                        var status = row.status;
                        switch(row.status){
                            case "on hold":
                                color = "gray";
                            break;
                            case "approved":
                                color = "lightgreen";
                            break;
                            case "rejected":
                                color = "deeporange";
                            break;
                            case "confirmed":
                                color = "green";
                            break;
                            case "cancelled":
                                color = "red";
                            break;
                            case "in progress":
                                color = "cyan";
                            break;
                            case "finalized":
                                color = "teal";
                            break;
                        }

                        status = "<button data-color='" + color + "' data-form='form-change-status' data-modal='modal-change-status' data-row-id='" + row.id + "' data-table='" + table + "' onclick='$.edit(event, $(this));' class='btn bgm-" + color + " btn-xs waves-effect'>" + row.status + "</button>";
                        return status;
                    }
                }
            });
        };

        $.message =     function(title, text, type){
            $.growl({
                icon: "fa fa-comments",
                title: '<b> ' + title + ' <b>',
                message: text,
                url: ''
                },{
                element: 'body',
                type: type,
                allow_dismiss: true,
                placement: {
                        from: "top",
                        align: "right"
                },
                offset: {
                    x: 20,
                    y: 85
                },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                url_target: '_blank',
                mouse_over: false,
                animate: {
                    enter: "animated flipInX",
                    exit: "animated flipOutX"
                },
                icon_type: 'class',
            });
        };

        $.update =      function(view){

            $.ajaxSetup({ cache: true, contentType: 'application/x-www-form-urlencoded; charset=UTF-8', processData: true });

            $.get('notifications', function(response){
                if(response!=0){
                    $('#new-requests').html(response);
                    $('#navigator-title').html('(' + response + ') {{ $company->name }}');
                    $('#new-requests').removeClass('invisible');
                    if(window.notification != response){
                        document.getElementById('audio-notification').play();
                        window.notification = response;
                    }
                }
                else{
                    $('#new-requests').addClass('invisible');
                    $('#navigator-title').html('{{ $company->name }}');
                };
            });

            if(window.update==true){

                $.get('startup', function(response){
                    var startup = JSON.stringify(response);
                    if(window.startup != startup){
                        var subsidiaries    = response.subsidiaries;
                        var $selects        = $('select.subsidiaries-select');
                        for (i=0;i<subsidiaries.length; i++) {
                            $selects.append($('<option>', {
                                value: subsidiaries[i].id,
                                text: subsidiaries[i].name
                            }));
                        }
                        $('.selectpicker').selectpicker('refresh');

                        window.startup = startup;
                    }
                });

                if(window.view != "" && window.view != "settings" && window.view != "home"){
                    $.get(window.view, function(response){
                        $.render(window.view, response);
                    });
                }
            };
        };

        $.render =      function(view, data){
            switch(view){
                case 'users':
                    var $users_list = $('#users-list');
                    var $admin_list = $('#admin-list');
                    $users_list.html("");
                    $admin_list.html("");

                    if (data.users.length<5) { var limit = data.users.length; }
                    else { var limit = 5; }
                    for (i=0; i < limit; i++) {
                        var user = data.users[i];
                        var image = (user.image == null) ? 'img/user.png' : 'storage/images/users/' + user.image;
                        var $user_item = $.tmpl($('#template'), { image: image, name: user.fullname, email: user.email, phone_number: user.phone_number, id: user.id});
                        $users_list.append($user_item);
                    }
                    if (data.admin.length<5) { var limit = data.admin.length; }
                    else { var limit = 5; }
                    for (i=0; i < limit; i++) {
                        var admin = data.admin[i];
                        var image = (admin.image == null) ? 'img/user.png' : 'storage/images/users/' + admin.image;
                        var $admin_item = $.tmpl($('#template'), { image: image, name: admin.fullname, email: admin.email, phone_number: admin.phone_number, id: admin.id});
                        $admin_list.append($admin_item);
                    }
                    var options = { colSelection: false };
                    $.datatable('admin', 'users', options);
                    $('#datatable-admin').bootgrid('clear');
                    $('#datatable-admin').bootgrid('append', data.admin);
                    $.datatable('users', 'users', options);
                    $('#datatable-users').bootgrid('clear');
                    $('#datatable-users').bootgrid('append', data.users);
                break;
                case 'reservations':
                    var options = {
                        colSelection: true
                    };
                    $.datatable('reservations', 'reservations', options);

                    var object = JSON.stringify(data);

                    if(object != window.current_object){
                        $('#datatable-reservations').bootgrid('clear');
                        $('#datatable-reservations').bootgrid('append', data);
                        window.current_object = object;
                    }
                break;
                case 'categories':
                    var $categories = $('#categories-table');
                    $categories.html("");

                    for (i=0;i<data.length;i++){
                        var $category = $.tmpl($('#tr-template'), { name: data[i].english_name, description: data[i].description, id: data[i].id});
                        $categories.append($category);
                    }
                break;
                case 'events':
                    var $events = $('#events-table');
                    $events.html("");

                    for (i=0;i<data.length;i++){
                        var subsidiary = data[i].subsidiary.name;
                        var $event = $.tmpl($('#tr-template-event'), { name: data[i].name, date: data[i].date, id: data[i].id, subsidiary: subsidiary });
                        $events.append($event);
                    }
                break;
            }
        };

        $.delete =      function(e, element){
            e.preventDefault();
            var form = $('#'+element.data('form'));
            var route = form.attr('action').replace(':UI', element.data('row-id'));
            var data = form.serialize();
            swal({
                title: "Are yur sure?",
                text: "You will not be able to retrieve this record.",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-primary",
                cancelButtonClass: "btn-info",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false
            }, function(){
                $.post(route, data, function(response){
                    if(response.type == 'success'){
                        $.update(window.view);
                        swal(response.title, response.message, response.type);
                    };

                    if(element.data('form')=='form-delete-items'){
                        $("#category-"+$(".group-id").val()).click();
                    };
                });
            });
        };

        $.edit =        function(e, element){
            e.preventDefault();
            $('div.form-group').removeClass('has-error');
            $('.help-block').html("");
            $('.btn-update').attr('data-id', element.data('row-id'));
            $('.btn-update').data('id', element.data('row-id'));
            $('.btn-update-file').attr('data-id', element.data('row-id'));
            $('.btn-update-file').data('id', element.data('row-id'));

            $.get(element.data('table')+"/"+element.data('row-id'), function(response){
                var form = $('#'+element.data('form'));
                $('#'+element.data('modal')).modal();
                $('#'+element.data('modal')).attr('data-modal-color', element.data('color'));
                for (index in response){
                    form.find('input[name='+index+'-u]').val(response[index]);
                    if(index=='subsidiary_id' || index=='subsidiary_id-u'){
                        $("select.subsidiaries-select").selectpicker('val', response[index]);
                    };
                    if(index=='book_type' || index=='book_type-u'){
                        $("select.book_type-select").selectpicker('val', response[index]);
                    };
                    if(index=='description' || index=='description-u'){
                        form.find('textarea[name='+index+'-u]').text(response[index]);
                    };
                    if(index=='image'){
                        $("."+element.data('table')+"-preview").attr('src', 'storage/images/'+element.data('table')+'/preview_'+response[index]);
                    };

                    if(index=='new' || index=='spicy' || index=='special' || index=='recommended'){
                        form.find('input[name='+index+'-u]').val(response[index]);
                        if (response[index]=="1"){
                        form.find('input[name='+index+'-u]').prop('checked', true);
                        }
                        else{
                            form.find('input[name='+index+'-u]').prop('checked', false);
                        }
                    };
                }
            });
        };

        $.elements =    function(e, element){
            e.preventDefault();
            $.get(element.data('table')+"/"+element.data('row-id'), function(response){
                var view = element.data('view');
                $('.views').css('display','none');
                $('#'+view).css('display','block');

                $("#breadcrumb-category").html(response.english_name);
                $("#thumbnail-card").attr('src', 'storage/images/categories/card_'+response.image);
                $("#name-card-english").html(response.english_name);
                $("#name-card-spanish").html(response.spanish_name);
                $("#description-card").html(response.description);
                $(".group-id").val(response.id);
                $(".name-card").html(response.english_name);

                var elements = response.elements;
                var $items = $('#items-table');
                $items.html("");

                for (i=0;i<elements.length;i++){

                    var nw = "";
                    var spicy = "";
                    var special = "";
                    var recommended = "";

                    if(elements[i].new=="1"){
                        nw = "New";
                    };
                    if(elements[i].spicy=="1"){
                        spicy = "Spicy";
                    };
                    if(elements[i].special=="1"){
                        special = "Special";
                    };
                    if(elements[i].recommended=="1"){
                        recommended = "Recommended";
                    };

                    var $item = $.tmpl($('#tr-template-item'), { id: elements[i].id , name: elements[i].english_name, price: elements[i].price, nw: nw, spicy: spicy, special: special, recommended: recommended });
                    $items.append($item);
                }
            });
        };

        $.user =        function(){
            $.get('users/'+{{ Auth::user()->id }}, function(response){
                $(".title-settings").html(response.first_name+" "+response.last_name+" - "+response.role);
                $(".t-first-name").html(response.first_name);
                $(".t-last-name").html(response.last_name);
                $(".t-address").html(response.address);
                $(".t-phone-number").html(response.phone_number);
                $(".t-email").html(response.email);
            });
        };

        $.details =     function(e, element){
            $.get(element.data('table')+"/"+element.data('row-id'), function(response){
                $('#'+element.data('modal')).modal();
                $('#'+element.data('modal')).attr('data-modal-color', element.data('color'));

                var prefix = "";
                if(element.data('table')=="items"){
                    var observations = "";
                    if(response.new=="1"){ observations += ' <i class="zmdi zmdi-circle c-lightgreen"></i>'; };
                    if(response.spicy=="1"){ observations += ' <i class="zmdi zmdi-circle c-red"></i>'; };
                    if(response.special=="1"){ observations += ' <i class="zmdi zmdi-circle c-amber"></i>'; };
                    if(response.recommended=="1"){ observations += ' <i class="zmdi zmdi-circle c-lightblue"></i>'; };
                    $('#details-observations').html(observations);
                    prefix = 'card_';
                }

                for (index in response) {
                    $('#details-'+index).html(response[index]);
                    if(index=="image" && response[index]!=null){ $('#details-image').attr('src', "storage/images/" + element.data('table') + "/" + prefix + response[index]); }
                    if(index=="favorites" || index=="likes"){
                        $('#details-'+index).html(response[index].length);
                    }
                }
            });
        };

        setInterval(function(){ $.update(window.view); }, 1000);
    });
</script>
