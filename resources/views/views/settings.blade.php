<div id="settings" class="views">

    <div class="block-header">
        <h2>Settings</h2>
    </div>

	<div class="card" id="profile-main">
        <div class="pm-overview c-overflow">
            <div class="pmo-pic">
                <div class="p-relative">
                    <img class="img-responsive img-profile" src="{{ Auth::user()->image == NULL ? 'img/user.png' : Auth::user()->image }}" alt="">
                    <div class="dropdown pmop-message">
                        <button type="button" class="btn bgm-white btn-float z-depth-1">
                            <i class="zmdi zmdi-account-circle"></i>
                        </button>
                    </div>
                    <a data-toggle="modal" href="#change-img" class="pmop-edit">
                        <i class="zmdi zmdi-camera"></i> <span class="hidden-xs">Update your profile picture</span>
                    </a>
                </div>
                <div class="pmo-stat" style="background-color: #1E97D0">
                    <h4 class="m-0 c-white title-settings">{{ Auth::user()->fullname }}</h4>
                </div>
                <a data-toggle="modal" href="#change-password" style="width: 100%; margin-top: 10px;" class="btn btn-md btn-info waves-effect">Change Password</a>
            </div>
        </div>

        <div class="pm-body clearfix">

            <div class="pmb-block">
                <div class="pmbb-header">
                    <h2><i class="zmdi zmdi-account m-r-5"></i> Basic Info</h2>
                    <ul class="actions">
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a data-pmb-action="edit" href="">Edit</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="pmbb-body p-l-30">
                    <div class="pmbb-view">
                        <dl class="dl-horizontal">
                            <dt>First Name</dt>
                            <dd class="t-first-name">{{ Auth::user()->first_name }}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Last Name</dt>
                            <dd class="t-last-name">{{ Auth::user()->last_name }}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Address</dt>
                            <dd class="t-address">{{ Auth::user()->address }}</dd>
                        </dl>
                    </div>

                    {{ Form::open(['url' => 'savebasic']) }}
                    	<input type="hidden" name="id" value="{{ Auth::user()->id }}">
	                    <div class="pmbb-edit">
	                        <dl class="dl-horizontal">
	                            <dt class="p-t-10">First Name</dt>
	                            <dd>
	                                <div class="fg-line">
	                                    <input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{ Auth::user()->first_name }}">
	                                </div>
	                            </dd>
	                        </dl>
	                        <dl class="dl-horizontal">
	                            <dt class="p-t-10">Last Name</dt>
	                            <dd>
	                                <div class="fg-line">
	                                    <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{ Auth::user()->last_name }}">
	                                </div>
	                            </dd>
	                        </dl>
	                        <dl class="dl-horizontal">
	                            <dt class="p-t-10">Address</dt>
	                            <dd>
	                                <div class="fg-line">
	                                    <input type="text" name="address" class="form-control" placeholder="Address" value="{{ Auth::user()->address }}">
	                                </div>
	                            </dd>
	                        </dl>

	                        <div class="m-t-30">
	                            <button type="button" class="btn btn-primary btn-sm btn-save-basic">Save</button>
	                            <button id="reset-basic" data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
	                        </div>
	                    </div>
                    {{ Form::close() }}
                </div>
            </div>

            <div class="pmb-block">
                <div class="pmbb-header">
                    <h2><i class="zmdi zmdi-phone m-r-5"></i> Contact Info</h2>
                    <ul class="actions">
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a data-pmb-action="edit" href="">Edit</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="pmbb-body p-l-30">
                    <div class="pmbb-view">
                        <dl class="dl-horizontal">
                            <dt>Phone Number</dt>
                            <dd class="t-phone-number">{{ Auth::user()->phone_number }}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Email</dt>
                            <dd class="t-email">{{ Auth::user()->email }}</dd>
                        </dl>
                    </div>
					{{ Form::open(['url' => 'savecontact']) }}
                    	<input type="hidden" name="id" value="{{ Auth::user()->id }}">
	                    <div class="pmbb-edit">
	                        <dl class="dl-horizontal">
	                            <dt class="p-t-10">Phone Number</dt>
	                            <dd>
	                                <div class="fg-line">
	                                    <input type="text" name="phone_number" class="form-control" placeholder="(0000) 000 0000" value="{{ Auth::user()->phone_number }}">
	                                </div>
	                            </dd>
	                        </dl>
	                        <dl class="dl-horizontal">
	                            <dt class="p-t-10">Email</dt>
	                            <dd>
	                                <div class="fg-line">
	                                    <input type="email" name="email" class="form-control" placeholder="example@example.com" value="{{ Auth::user()->email }}">
	                                </div>
	                            </dd>
	                        </dl>
	                        <div class="m-t-30">
	                            <button type="button" class="btn btn-primary btn-sm btn-save-contact">Save</button>
	                            <button id="reset-contact" data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
	                        </div>
	                    </div>
					{{ Form::close() }}
                </div>
            </div>
        </div>
    </div>









    <div class="modal fade in" id="change-password" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="border-radius: 3px 3px 8px 8px;">
                <div class="modal-header">
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'change', 'id' => 'form-change-password']) !!}
                        <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                        <div class="form-group fg-line">
							<input type="password" name="old_password" class="form-control " placeholder="Current Password">
                        </div>
                        <div class="form-group fg-line">
							<input type="password" name="password" class="form-control " placeholder="New Password">
                        </div>
                        <div class="form-group fg-line">
							<input type="password" name="password_confirmation" class="form-control " placeholder="Password Confirmation">
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer" style="background-color: #1E87DB; border-radius: 0px 0px 4px 4px;">
                    <button type="button" class="btn btn-primary btn-save" data-form="form-change-password">Save</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="change-img" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="border-radius: 3px 3px 8px 8px;">
                <div class="modal-header">
                    <h4 class="modal-title">Change Picture</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'changeimage/:UI', 'id' => 'form-change-img', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group fg-line">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-primary btn-file m-r-10">
                                    <span class="fileinput-new">Select File</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="image" accept="image/*">
                                </span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                            </div>
                        </div>
                        <small class="help-block c-red text-left" id="error-image"></small>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer" style="background-color: #1E87DB; border-radius: 0px 0px 4px 4px;">
                    <button type="button" class="btn btn-primary btn-image" data-form="form-change-img">Save</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



</div>

