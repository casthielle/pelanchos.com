<div id="items" class="views">

	<div class="block-header">
        <ol class="breadcrumb">
            <li>MENU</li>
            <li><a href="#" class="breadcrumb-back" data-back="categories">Categories</a></li>
            <li class="active" id="breadcrumb-category"></li>
        </ol>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="card blog-post">
                <div class="bp-header">

                    {!! Html::image('','preview', ['id' => 'thumbnail-card']) !!}

                    <a href="" class="bp-title bgm-red" style="height:81px;">
                        <h2 id="name-card-english"></h2>
                        <small id="name-card-spanish"></small>
                    </a>
                </div>
                <div class="p-20 c-overflow" id="description-card" style="height: 169px;"></div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="card">
                <div class="card-header ch-alt m-b-20">
                    <h2>Items of <span class="name-card"></span> <small>List of menu items.</small></h2>
                    <button data-toggle="modal" href="#modal-add-items" class="btn bgm-red btn-float btn-with-color" data-target-color="blue"><i class="zmdi zmdi-plus"></i></button>
                </div>

                <div class="card-body">

                    <div class="table-responsive items-div c-overflow">
                        <table class="table table-hover table-condensed table-vmiddle text-center">
                            <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Observatios</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="items-table">
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>