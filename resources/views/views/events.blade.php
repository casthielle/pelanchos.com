<div id="events" class="views">

    <div class="block-header">
        <ol class="breadcrumb">
            <li>EVENTS</li>
        </ol>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header ch-alt">
                    <h2>Add Event <small>Form to add a new event.</small></h2>
                </div>

                <div class="card-body card-padding">
                    {!! Form::open(['route' => 'events.store', 'id' => 'form-add-events', 'files' => 'true']) !!}
                        <div class="form-group fg-line m-b-0">
                            <input type="text" name="date" class="form-control date-picker" placeholder="Date" autocomplete="off" value="{{ date('d/m/Y') }}">
                        </div>
                        <small class="help-block c-red text-left" id="error-date"></small>

                        <div class="form-group fg-line m-b-0">
                            <input type="text" name="name" class="form-control" placeholder="Name">
                        </div>
                        <small class="help-block c-red text-left" id="error-name"></small>

                        <div class="form-group fg-line m-b-0">
                            <select name="subsidiary_id" class="selectpicker subsidiaries-select" title='SUBSIDIARY'>
                            </select>
                        </div>
                        <small class="help-block c-red text-left" id="error-subsidiary_id"></small>

                        <div class="form-group fg-line m-b-0">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-primary btn-file m-r-10">
                                    <span class="fileinput-new">Select Image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="image">
                                </span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                            </div>
                        </div>
                        <small class="help-block c-red text-left" id="error-image"></small>

                        <div class="form-group fg-line m-b-20">
                            <textarea name="details" class="form-control auto-size input-sm" placeholder="Details"></textarea>
                        </div>
                        <small class="help-block c-red text-left" id="error-details"></small>

                        <button class="btn btn-info btn-save-file" data-form="form-add-events">Save</button>
                        <button type="button" class="btn btn-link btn-cancel">Cancel</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="card">
                <div class="card-header bgm-amber">
                    <h2><i class="zmdi zmdi-star f-20 c-yellow"></i> &nbsp;Events <small>listing of events.</small></h2>
                </div>

                <div class="table-responsive categories-div c-overflow">
                    <table class="table table-hover table-vmiddle text-center">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Date</th>
                                <th class="text-center">Subsidiary</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="events-table">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>