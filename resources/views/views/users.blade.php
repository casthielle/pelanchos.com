<div id="users" class="views">

	<div class="block-header">
        <h2>Users</h2>
        <ul class="actions">
            <li>
                <a href="">
                    <i class="zmdi zmdi-trending-up"></i>
                </a>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header bgm-bluegray ch-alt m-b-20">
                    <h2>Customers <small>List of customers using the app.</small></h2>
                    <ul class="actions actions-alt">
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="">Download the list</a>
                                </li>
                                <li>
                                    <a href="">Send the list via email</a>
                                </li>
                                <li>
                                    <a href="">Send email to all customers</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="card-body">
                    <div class="listview">
                        <div id="users-list">

                        </div>
                        <a class="lv-footer" data-toggle="modal" href="#modal-all-users">View All</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="card">
                <div class="card-header ch-alt m-b-20">
                    <h2>Administrators & Operators <small>list of administrators and operators of the system.</small></h2>
                    <ul class="actions">
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="">Download the list</a>
                                </li>
                                <li>
                                    <a href="">send the list via email</a>
                                </li>
                                <li>
                                    <a href="">Send email to all customers</a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <button data-toggle="modal" href="#modal-add-admin" class="btn bgm-cyan btn-float btn-with-color" data-target-color="lightblue"><i class="zmdi zmdi-plus"></i></button>
                </div>

                <div class="card-body">
                    <div class="listview">
                        <div id="admin-list">

                        </div>
                        <a class="lv-footer" data-toggle="modal" href="#modal-all-admin">View All</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>