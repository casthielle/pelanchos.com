<div id="reservations" class="views">

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header ch-alt m-b-20">
                    <h2>Reservations <small>List of reservations.</small></h2>
                    <button data-toggle="modal" href="#modal-add-reservations" class="btn bgm-red btn-float btn-with-color" data-target-color="red"><i class="zmdi zmdi-plus"></i></button>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                    <table id="datatable-reservations" class="table table-hover table-condensed table-vmiddle bootgrid-table">
                        <thead>
                            <tr>
                                <th data-column-id="date">Date</th>
                                <th data-column-id="name">Name</th>
                                <th data-column-id="email" data-visible="false">Email</th>
                                <th data-column-id="phone_number">Phone Number</th>
                                <th data-column-id="book_type" data-visible="false">Book Type</th>
                                <th data-column-id="subsidiary_name">Subsidiary</th>
                                <th data-column-id="status" data-formatter="status" data-sortable="false" data-align="center" data-header-align="center">Status</th>
                                <th data-column-id="commands" data-formatter="commands" data-sortable="false" data-searchable="false" data-align="center" data-header-align="center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>