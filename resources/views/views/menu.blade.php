<div id="categories" class="views">

	<div class="block-header">
        <ol class="breadcrumb">
            <li>MENU</li>
            <li class="active">Categories</li>
        </ol>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header ch-alt">
                    <h2>Add Category <small>Form to add a new category.</small></h2>
                </div>

                <div class="card-body card-padding">
                    {!! Form::open(['route' => 'categories.store', 'id' => 'form-add-categories', 'files' => 'true']) !!}
                    <div class="form-group fg-line m-b-0">
                        <input type="text" name="english_name" class="form-control" placeholder="Name (English)">
                    </div>
                    <small class="help-block c-red text-left" id="error-english_name"></small>

                    <div class="form-group fg-line m-b-0">
                        <input type="text" name="spanish_name" class="form-control" placeholder="Name (Spanish)">
                    </div>
                    <small class="help-block c-red text-left" id="error-spanish_name"></small>

                    <div class="form-group fg-line m-b-0">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-primary btn-file m-r-10">
                                <span class="fileinput-new">Select Image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="image">
                            </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                        </div>
                    </div>
                    <small class="help-block c-red text-left" id="error-image"></small>

                    <div class="form-group fg-line m-b-20">
                        <textarea name="description" class="form-control auto-size input-sm" placeholder="Description"></textarea>
                    </div>
                    <small class="help-block c-red text-left" id="error-description"></small>

                    <button class="btn btn-info btn-save-file" data-form="form-add-categories">Save</button>
                    <button type="button" class="btn btn-link btn-cancel">Cancel</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="card">
                <div class="card-header card-alt bgm-bluegray">
                    <h2>Categories <small>listing of menu categories.</small></h2>
                </div>

                <div class="table-responsive categories-div c-overflow">
                    <table class="table table-hover table-vmiddle text-center">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="categories-table">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>