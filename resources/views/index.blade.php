<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Joel Crespo">
    <title>Pelanchos | Mexican Grill</title>

    <!--CSS-->
    {!! Html::style('css/index.css') !!}

    <!--Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,700,800,100,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

    <!--Icons-->
    <link rel="shortcut icon" href="images/ico/favicon.png">
    <!--<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">-->
    <!--<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">-->
    <!--<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">-->
    <!--<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">-->
</head>

<body>
    <div id="bg-slider-section">
        <div id="slider-section">
            <div class="overlay-bg"></div>
            <div class="container">
                <div class="main-slider">
                    <div class="slider-content text-center">
                        <div class="slider-logo">
                            <img class="img-responsive" style="width: 90px;" src="images/others/logo.svg" alt="" />
                        </div>
                        <h1><span class="pacifico">Welcome To</span> <span class="company">{{ strtoupper($company->name) }}</span></h1>
                        <h2>Ready To Visit Old Mexico...</h2>
                        <a href="#table-booking" class="btn btn-main">Book Your Table Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/#bg-video-section-->

    <div id="special-items">
        <div class="container">
            <div class="text-center padding">
                <div class="row section-title">
                    <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                        <h1 class="text-danger">Our Speciality</h1>
                        <p>Discover the best of old mexico in the company of family and friends.</p>
                    </div>
                </div>
                <div class="row special-items">
                    <div class="col-sm-4">
                        <div class="special-item">
                            <div class="special-item-image">
                                <img class="img-responsive" src="images/others/sp-item1.jpg" alt="" />
                                <img class="img-responsive" src="images/others/sp-item3.jpg" alt="" />
                            </div>
                            <h2>Serving With Love</h2>
                            <p>Food prepared with love and care by expert chefs in Mexican cuisine</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="special-item">
                            <div class="special-item-image">
                                <img class="img-responsive" src="images/others/sp-item2.jpg" alt="" />
                                <img class="img-responsive" src="images/others/sp-item1.jpg" alt="" />
                            </div>
                            <h2>Tasty Products</h2>
                            <p>Delicious flavors from Mexican cuisine. The best atmosphere in the style of old Mexico.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="special-item">
                            <div class="special-item-image">
                                <img class="img-responsive" src="images/others/sp-item3.jpg" alt="" />
                                <img class="img-responsive" src="images/others/sp-item1.jpg" alt="" />
                            </div>
                            <h2>Wide Range Flavors</h2>
                            <p>All Food Prepared on Premises Steaks, Seafood, and Vegetarian Beer, Wine and Margaritas</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/#special-items-->

    <div id="our-products" class="parallax-section">
        <div class="overlay-bg"></div>
        <div class="padding text-center">
            <div class="container">
                <div class="row section-title">
                    <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                        <h1>Food We Serve</h1>
                        <p>Enjoy Mexican cuisine at any time of day</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="our-product">
                            <img class="img-responsive" src="images/others/item1.png" alt="" />
                            <h2>Breakfast</h2>
                            <p>Start the day with a good Mexican breakfast. </p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="our-product">
                            <img class="img-responsive" src="images/others/item2.png" alt="" />
                            <h2>Brunch</h2>
                            <p>Gaze with high quality food. <br><br></p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="our-product">
                            <img class="img-responsive" src="images/others/item3.png" alt="" />
                            <h2>Lunch/Dinner</h2>
                            <p>Nothing better than a good Mexican food to replenish energy during the day.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/#our-products-->


    <div id="our-menu">
        <div class="container text-center">
            <div class="row section-title">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                    <h1 class="text-danger">Our Menu</h1>
                    <p>Come and Experience Our Old Mexican Atmosphere, Surrounded by Colorful, Authentic Decor. Hand-Crafted Items Surround Our Enjoyable Setting Step Into Mexico and ENJOY!</p>
                </div>
            </div>
        </div>
        <div class="text-center panel-group" id="menu-accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div id="beverages" class="panel-collapse collapse in">
                    <div class="container panel-body">
                        <div class="menu-items">
                            <div class="row">

                                @foreach ($menu as $item)

                                    <div class="col-sm-4">
                                        <div class="item text-center">

                                            @if ($item->image != NULL)
                                                {!! Html::image('storage/images/items/preview_'.$item->image, 'item image', ['class'=>'img-responsive item-page']) !!}
                                            @else
                                                <img class="img-responsive" src="images/menu/menu-default.png" alt="" />
                                            @endif

                                            <h2>{{ $item->english_name }}</h2>
                                            <span>${{ $item->price }}</span>
                                            <p>{{ $item->description }}</p>
                                        </div>
                                    </div>

                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/panel-->
        </div>
        <div class="container text-center">
            <div class="row section-title">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                    <h1 class="text-danger">Our App</h1>
                    <p>See our full menu with our new app. Find her already in Google play store</p>
                    <br>
                    <a target="_blank" href='http://play.google.com/store?utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'>
                    <img style="width:200px;" alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/>
                    </a>
                </div>
            </div>
        </div>
    </div><!--/#our-menu-->

    <div id="extras" class="parallax-section">
        <div class="overlay-bg"></div>
        <div class="container text-center">
            <div class="extra-benefits">
                <h1><span class="company">PELANCHOS</span> Extra’s</h1>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="extra">
                            <img src="images/others/musicnote.png" style="width:60px;"alt="">
                            <h2>Live Entertainment</h2>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="extra">
                            <img src="images/others/dog.png" style="width:80px;"alt="">
                            <h2>Dog Place</h2>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="extra">
                            <img src="images/others/ticket.png" style="width:70px;"alt="">
                            <h2>Raffles</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/#extras-->

    <div id="table-booking">
        <div class="container">
            <div class="padding contact-form text-center">
                <div class="row section-title">
                    <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                        <h1 class="text-danger">Book a table</h1>
                        <p>Book a table in Pelanchos now, and enjoy good Mexican food.</p>
                    </div>
                </div>
                {!! Form::open(['url' => 'reserve', 'id' => 'reservation-form', 'name' => 'contact-form']) !!}
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <input type="text" name="name" class="form-control" required="required" placeholder="Name">
                            <small class="help-block c-red text-left" id="error-name"></small>
                        </div>

                        <div class="form-group col-sm-4">
                            <input type="email" name="email" class="form-control" required="required" placeholder="Your Email">
                            <small class="help-block c-red text-left" id="error-email"></small>
                        </div>

                        <div class="form-group col-sm-4">
                            <input type="number" name="table_size" class="form-control" placeholder="Table Size">
                            <small class="help-block c-red text-left" id="error-table_size"></small>
                        </div>

                        <div class="form-group col-sm-6">
                            <input type="tel" name="phone_number" class="form-control" placeholder="Mobile Number">
                            <small class="help-block c-red text-left" id="error-phone_number"></small>
                        </div>

                        <div class="form-group col-sm-6">
                            <input class="date-pick form-control" data-date-format="mm/dd/yyyy" type="text" id="check_in" name="date" placeholder="Date">
                            <small class="help-block c-red text-left" id="error-date"></small>
                        </div>

                        <div class="form-group col-sm-6">
                            <div class="form-group room_type-select">
                                <select class="form-control" name="book_type" id="room_type">
                                    <option value="">Book Type</option>
                                    <option value="Breakfast">Breakfast</option>
                                    <option value="Lunch">Lunch</option>
                                    <option value="Dinner">Dinner</option>
                                </select>
                            </div>
                            <small class="help-block c-red text-left" id="error-book_type"></small>
                        </div>

                        <div class="form-group col-sm-6">
                            <div class="form-group subsidiary_id-select">
                                <select class="form-control" name="subsidiary_id" id="subsidiary_id">
                                    <option value="">Subsidiary</option>
                                    @foreach($subsidiaries as $subsidiary)
                                    <option value="{{ $subsidiary->id }}">{{ $subsidiary->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <small class="help-block c-red text-left" id="error-subsidiary_id"></small>
                        </div>

                        <div class="form-group col-sm-12">
                            <textarea name="requirements" id="message" class="form-control" rows="8" placeholder="Enter Your Requirements"></textarea>
                            <small class="help-block c-red text-left" id="error-requirements"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="button" data-form="reservation-form" class="btn btn-submit btn-save">Submit</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div><!--/#table-booking-->

    <div id="comments" class="parallax-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="comment bg-danger-transparent">
                        <img class="img-responsive" src="images/others/quote.png" alt="" />
                        <h2>People Are Saying</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <h4>Tanya Blazelist, <span>Master Chef</span></h4>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="commenter text-center">
                        <img class="img-responsive" src="images/others/commentor.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div><!--/#comments-->

    <div id="contact">
        <div class="container">
            <div class="padding contact-content">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="contact-info">
                            <img class="img-responsive footer-logo" style="width: 90px;" src="images/others/logo-border.svg" alt="" />

                            @foreach ($subsidiaries as $subsidiary)

                            <h3><b>{{ $subsidiary->name }}</b></h3>
                            <ul class="address">
                                <li><i class="fa fa-map-marker"></i>{{ $subsidiary->location }}</li>
                                <li><i class="fa fa-phone"></i><a href="tel:{{ $subsidiary->phone_number }}">{{ $subsidiary->phone_number }}</a></li>
                                <li><i class="fa fa-envelope"></i>{{ $subsidiary->email }}</li>
                            </ul>

                            @endforeach

                            <ul class="list-inline social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="contact-info date">
                            <img class="img-responsive" src="images/others/clock.png" alt="" />
                            <h2>Working Hours</h2>
                            <h3><b>Knoxville</b></h3>
                            <ul>
                                <li>Monday - Thursday ................. 11 am - 10 pm</li>
                                <li>Friday - Saturday ....................... 11 am - 11 pm</li>
                                <li>Sunday .............................................. 11 am - 10 pm</li>
                            </ul>

                            <h3><b>Seymour</b></h3>
                            <ul>
                                <li>Monday - Thursday ................. 11 am - 09 pm</li>
                                <li>Friday - Saturday ....................... 11 am - 10 pm</li>
                                <li>Sunday .............................................. 11 am - 10 pm</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="contact-info">
                            <img class="img-responsive" src="images/others/calender.png" alt="" />
                            <h2>Ucomming Events</h2>

                            @foreach($events as $event)
                            <div class="event">
                                <div class="event-date">
                                    <p><span>{{ explode('-', $event->date)[0] }}</span> {{ explode('-', $event->date)[1] }}</p>
                                </div>
                                <div class="event-info">
                                    <h4>{{ $event->name }}</h4>
                                    <p>(in {{ $event->subsidiary->name }}) {{ $event->details }}</p>
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/#contact-->







    <footer>
        <div id="gmap" style="width:50%;float:left;"></div>
        <div id="gmap2" style="width:50%;"></div>

        <div id="footer-bottom" class="bg-danger">
            <div class="container text-center">
                <p>© 2015 <a href="#">Pelancho's</a>. All Rights Reserved.</p>
            </div>
        </div>
    </footer>






    <!--/#scripts-->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyA_SJrFB8I4dWJH5c6aSvCu4Z6h8WXGB6c"></script>

    {!! Html::script('js/index.js') !!}

    <script>
        $('input.date-pick').datepicker();
        $(document).ready(function(){
            $('body').on('click', '.btn-save', function(e){
                e.preventDefault();
                var btn = $(this);
                var form  =  $('#'+btn.data('form'));
                var url   =  form.attr('action');
                var data  =  form.serialize();
                $.post(url, data, function(response){
                    $('div.form-group').removeClass('has-error');
                    $('.help-block').html("");
                    if(response.type){
                        swal(response.title, response.message, response.type);

                        swal({
                          title: response.title,
                          text: response.message,
                          type: response.type,
                          showCancelButton: false,
                          confirmButtonClass: "btn btn-submit",
                          confirmButtonText: "Ok"
                        })

                        form.each(function(){
                            this.reset();
                        });
                    }
                    else{
                        for (index in response){
                            $('#'+btn.data('form')+' input[name='+index+']').parents('div.form-group').addClass('has-error');
                            $('#'+btn.data('form')+' small#error-'+index).html(response[index]);
                            if(index=="subsidiary_id" || index=="book_type"){
                                $('#'+btn.data('form')+' select[name='+index+']').parents('div.form-group').addClass('has-error');
                            }
                        }
                    }
                });
            });
        });
    </script>


</body>
</html>