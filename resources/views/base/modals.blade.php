
<!-- ADD USER MODAL -->

<div class="modal fade in" id="modal-add-admin" data-modal-color="" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bgm-white">
                <h4 class="modal-title c-gray">New User</h4>
            </div>
            <div class="modal-body bgm-white">
                {!! Form::open(['route' => 'users.store', 'id' => 'form-add-users']) !!}
                    <div class="form-group fg-line">
                        <input maxlength="50" type="text" name="first_name" class="form-control" placeholder="First Name">
                    </div>
                    <small class="help-block c-red text-left" id="error-first_name"></small>

                    <div class="form-group fg-line">
                        <input maxlength="50" type="text" name="last_name" class="form-control" placeholder="Last Name">
                    </div>
                    <small class="help-block c-red text-left" id="error-last_name"></small>

                    <div class="form-group fg-line">
                        <input maxlength="20" type="text" name="phone_number" class="form-control input-mask" data-mask="(000) 000-0000" placeholder="Phone Number" maxlength="15" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-phone_number"></small>

                    <div class="form-group fg-line">
                        <input type="email" name="email" class="form-control" placeholder="Email" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-email"></small>

                    <div class="form-group fg-line">
                        <input type="text" name="address" class="form-control" placeholder="Address" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-address"></small>

                    <div class="form-group fg-line">
                        <select name="role" class="selectpicker" title='ROLE'>
                            <option value="administrator">Administrator</option>
                            <option value="operator">Operator</option>
                        </select>
                    </div>
                    <small class="help-block c-red text-left" id="error-type"></small>

                    <div class="form-group fg-line">
                        <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-password"></small>

                    <div class="form-group fg-line">
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Password Confirmation" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-password_confirmation"></small>


                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-save" data-form="form-add-users">Save</button>
                <button type="button" class="btn btn-link btn-cancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<!-- VIEW ALL ADMIN MODAL -->

<div class="modal fade in" id="modal-all-admin" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">All Administrators</h4>
            </div>
            <div class="modal-body">

				<div class="table-responsive">
	            	<table id="datatable-admin" class="table table-hover table-condensed table-vmiddle bootgrid-table">
	                    <thead>
	                        <tr>
	                            <th data-column-id="fullname">Name</th>
	                            <th data-column-id="phone_number">Phone Number</th>
	                            <th data-column-id="email">Email</th>
	                            <th data-column-id="commands" data-formatter="commands" data-sortable="false" data-align="center" data-header-align="center" data-searchable="false">Acciones</th>
	                        </tr>
	                    </thead>
	                    <tbody>

	                    </tbody>
	                </table>
				</div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- VIEW ALL USERS MODAL -->

<div class="modal fade in" id="modal-all-users" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">All Customers</h4>
            </div>
            <div class="modal-body">
				<div class="table-responsive">
	            	<table id="datatable-users" class="table table-hover table-condensed table-vmiddle bootgrid-table">
	                    <thead>
	                        <tr>
	                            <th data-column-id="fullname">Name</th>
	                            <th data-column-id="phone_number">Phone Number</th>
	                            <th data-column-id="email">Email</th>
	                            <th data-column-id="commands" data-formatter="commands" data-sortable="false" data-align="center" data-header-align="center" data-searchable="false">Acciones</th>
	                        </tr>
	                    </thead>
	                    <tbody>

	                    </tbody>
	                </table>
				</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- EDIT USER MODAL -->

<div class="modal fade in" id="modal-edit-users" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bgm-white">
                <h4 class="modal-title c-gray">Edit User</h4>
            </div>
            <div class="modal-body bgm-white">
                {!! Form::open(['route' => ['users.update', ':UI'], 'method' => 'PUT', 'id' => 'form-edit-users']) !!}

                    <div class="form-group fg-line">
                        <input type="text" name="first_name-u" class="form-control" placeholder="First Name">
                    </div>
                    <small class="help-block c-red text-left" id="error-first_name-u"></small>

                    <div class="form-group fg-line">
                        <input type="text" name="last_name-u" class="form-control" placeholder="Last Name">
                    </div>
                    <small class="help-block c-red text-left" id="error-last_name-u"></small>

                    <div class="form-group fg-line">
                        <input type="text" name="phone_number-u" class="form-control input-mask" data-mask="(0000) 000-0000" placeholder="Phone Number" maxlength="15" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-phone_number-u"></small>

                    <div class="form-group fg-line">
                        <input type="email" name="email-u" class="form-control" placeholder="Email" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-email-u"></small>

                    <div class="form-group fg-line">
                        <input type="text" name="address-u" class="form-control" placeholder="Address" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-address-u"></small>

                    <small class="help-block c-red text-left" id="error-type-u"></small>

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-update" data-form="form-edit-users">Save</button>
                <button type="button" class="btn btn-link btn-cancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<!-- BOOK A TABLE MODAL -->

<div class="modal fade in" id="modal-add-reservations" data-modal-color="" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bgm-white">
                <h4 class="modal-title c-gray">Book a Table</h4>
            </div>
            <div class="modal-body bgm-white">
                {!! Form::open(['route' => 'reservations.store', 'id' => 'form-add-reservations']) !!}
                    <div class="form-group fg-line">
                        <input type="text" name="date" class="form-control date-picker" placeholder="Date" autocomplete="off" value="{{ date('d/m/Y') }}">
                    </div>
                    <small class="help-block c-red text-left" id="error-date"></small>

                    <div class="form-group fg-line">
                        <input maxlength="50" type="text" name="name" class="form-control" placeholder="Name">
                    </div>
                    <small class="help-block c-red text-left" id="error-name"></small>

                    <div class="form-group fg-line">
                        <input type="email" name="email" class="form-control" placeholder="Email" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-email"></small>

                    <div class="form-group fg-line">
                        <input maxlength="20" type="text" name="phone_number" class="form-control input-mask" data-mask="(000) 000-0000" placeholder="Phone Number" maxlength="15" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-phone_number"></small>

                    <div class="form-group fg-line">
                        <input type="text" name="table_size" class="form-control" placeholder="Table Size" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-table_size"></small>

                    <div class="form-group fg-line">
                        <select name="book_type" class="selectpicker book_type-select" title='BOOK TYPE'>
                            <option value="breakfast">Breakfast</option>
                            <option value="lunch">Lunch</option>
                            <option value="dinner">Dinner</option>
                        </select>
                    </div>
                    <small class="help-block c-red text-left" id="error-book_type"></small>

                    <div class="form-group fg-line">
                        <select name="subsidiary_id" class="selectpicker subsidiaries-select" title='SUBSIDIARY'>
                        </select>
                    </div>
                    <small class="help-block c-red text-left" id="error-subsidiary_id"></small>

                    <div class="form-group fg-line">
                    	<textarea name="requirements" class="form-control auto-size" placeholder="Others Requirements..."></textarea>
                    </div>
                    <small class="help-block c-red text-left" id="error-requirements"></small>


                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-save" data-form="form-add-reservations">Save</button>
                <button type="button" class="btn btn-link btn-cancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<!-- EDIT RESERVATION MODAL -->

<div class="modal fade in" id="modal-edit-reservations" data-modal-color="" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bgm-white">
                <h4 class="modal-title c-gray">Edit Reservation</h4>
            </div>
            <div class="modal-body bgm-white">
                {!! Form::open(['route' => ['reservations.update', ':UI'], 'method' => 'PUT', 'id' => 'form-edit-reservations']) !!}
                    <div class="form-group fg-line">
                        <input type="text" name="date-u" class="form-control date-picker" placeholder="Date" autocomplete="off" value="{{ date('d/m/Y') }}">
                    </div>
                    <small class="help-block c-red text-left" id="error-date-u"></small>

                    <div class="form-group fg-line">
                        <input maxlength="50" type="text" name="name-u" class="form-control" placeholder="Name">
                    </div>
                    <small class="help-block c-red text-left" id="error-name-u"></small>

                    <div class="form-group fg-line">
                        <input type="email" name="email-u" class="form-control" placeholder="Email" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-email-u"></small>

                    <div class="form-group fg-line">
                        <input maxlength="20" type="text" name="phone_number-u" class="form-control input-mask" data-mask="(000) 000-0000" placeholder="Phone Number" maxlength="15" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-phone_number-u"></small>

                    <div class="form-group fg-line">
                        <input type="text" name="table_size-u" class="form-control" placeholder="Table Size" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-table_size-u"></small>

                    <div class="form-group fg-line">
                        <select name="book_type-u" class="selectpicker book_type-select" title='BOOK TYPE'>
                            <option value="breakfast">Breakfast</option>
                            <option value="lunch">Lunch</option>
                            <option value="dinner">Dinner</option>
                        </select>
                    </div>
                    <small class="help-block c-red text-left" id="error-book_type-u"></small>

                    <div class="form-group fg-line">
                        <select name="subsidiary_id-u" class="selectpicker subsidiaries-select" title='SUBSIDIARY'>
                        </select>
                    </div>
                    <small class="help-block c-red text-left" id="error-subsidiary_id-u"></small>

                    <div class="form-group fg-line">
                        <textarea name="requirements-u" class="form-control auto-size" placeholder="Others Requirements..."></textarea>
                    </div>
                    <small class="help-block c-red text-left" id="error-requirements-u"></small>


                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-update" data-form="form-edit-reservations">Save</button>
                <button type="button" class="btn btn-link btn-cancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<!-- CHANGE RESERVATION STATUS MODAL -->

<div class="modal fade in" id="modal-change-status" data-modal-color="" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bgm-white">
                <h4 class="modal-title c-gray">Change Status</h4>
            </div>
            <div class="modal-body bgm-white">
                {!! Form::open(['route' => ['reservations.update', ':UI'], 'method' => 'PUT', 'id' => 'form-change-status']) !!}

                    <input type="hidden" name="change" value=1>

                    <div class="form-group fg-line">
                        <select name="status-u" class="selectpicker" title='STATUS'>
                            <option value="on hold">On Hold</option>
                            <option value="approved">Approved</option>
                            <option value="rejected">Rejected</option>
                            <option value="confirmed">Confirmed</option>
                            <option value="cancelled">Cancelled</option>
                            <option value="in progress">In Progress</option>
                            <option value="finalized">Finalized</option>
                        </select>
                    </div>
                    <small class="help-block c-red text-left" id="error-book_type-u"></small>

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-update" data-form="form-change-status">Save</button>
                <button type="button" class="btn btn-link btn-cancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<!-- EDIT CATEGORY MODAL -->

<div class="modal fade in" id="modal-edit-categories" data-modal-color="" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bgm-white">
                <h4 class="modal-title c-gray">Edit Category</h4>
            </div>
            <div class="modal-body bgm-white">
                {!! Form::open(['route' => ['categories.update', ':UI'], 'method' => 'PUT', 'id' => 'form-edit-categories', 'files' => 'true']) !!}
                    <div class="form-group fg-line m-b-20">
                        <input type="text" name="english_name-u" class="form-control input-sm" placeholder="Name (English)">
                    </div>
                    <small class="help-block c-red text-left" id="error-english_name-u"></small>

                    <div class="form-group fg-line m-b-20">
                        <input type="text" name="spanish_name-u" class="form-control input-sm" placeholder="Name (Spanish)">
                    </div>
                    <small class="help-block c-red text-left" id="error-spanish_name-u"></small>

                    <div class="form-group fg-line m-b-20 col-md-4">
                        <div class="thumbnail">
                            {!! Html::image('','Preview', ['class' => 'categories-preview image-preview']) !!}
                        </div>
                    </div>

                    <div class="form-group fg-line m-b-20">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-primary btn-file m-r-10">
                                <span class="fileinput-new">Select Image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="image">
                            </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                        </div>
                    </div>
                    <small class="help-block c-red text-left" id="error-image"></small>

                    <div class="form-group fg-line m-b-20">
                        <textarea name="description-u" class="form-control auto-size input-sm" placeholder="Description"></textarea>
                    </div>
                    <small class="help-block c-red text-left" id="error-description-u"></small>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-update-file" data-form="form-edit-categories">Save</button>
                <button type="button" class="btn btn-link btn-cancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<!-- ADD ITEM MODAL -->

<div class="modal fade in" id="modal-add-items" data-modal-color="" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bgm-white">
                <h4 class="modal-title c-gray">New Item</h4>
            </div>
            <div class="modal-body bgm-white">
                {!! Form::open(['route' => 'items.store', 'id' => 'form-add-items', 'files' => 'true']) !!}

                    {!! Form::hidden('group_id', null, ['class' => 'group-id']) !!}

                    <div class="form-group fg-line">
                        <input maxlength="50" type="text" name="english_name" class="form-control" placeholder="Name (English)">
                    </div>
                    <small class="help-block c-red text-left" id="error-english_name"></small>

                    <div class="form-group fg-line">
                        <input maxlength="50" type="text" name="spanish_name" class="form-control" placeholder="Name (Spanish)">
                    </div>
                    <small class="help-block c-red text-left" id="error-spanish_name"></small>

                    <div class="form-group fg-line">
                        <input maxlength="20" type="text" name="position" class="form-control" placeholder="Position" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-position"></small>

                    <div class="form-group fg-line">
                        <input maxlength="20" type="text" name="price" class="form-control" placeholder="Price" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-price"></small>

                    <div class="form-group fg-line">
                        <input maxlength="20" type="text" name="offer" class="form-control" placeholder="Offer" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-offer"></small>

                    <div class="form-group fg-line m-b-20">
                        <textarea name="description" class="form-control auto-size input-sm" placeholder="Description"></textarea>
                    </div>
                    <small class="help-block c-red text-left" id="error-description"></small>

                    <div class="form-group fg-line m-b-20">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-primary btn-file m-r-10">
                                <span class="fileinput-new">Select Image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="image">
                            </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                        </div>
                    </div>
                    <small class="help-block c-red text-left" id="error-image"></small>

                    <div class="row">
                        <div class="col-sm-6 m-b-20">
                            <div class="toggle-switch" data-ts-color="green">
                                <label for="ts1" class="ts-label">New</label>
                                <input id="ts1" name="new" value="0" class="custom-checkbox" type="checkbox" hidden="hidden">
                                <label for="ts1" class="ts-helper"></label>
                            </div>
                        </div>
                        <div class="col-sm-6 m-b-20">
                            <div class="toggle-switch" data-ts-color="red">
                                <label for="ts2" class="ts-label">Spicy</label>
                                <input id="ts2" name="spicy" value="0" class="custom-checkbox" type="checkbox" hidden="hidden">
                                <label for="ts2" class="ts-helper"></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 m-b-20">
                            <div class="toggle-switch" data-ts-color="amber">
                                <label for="ts3" class="ts-label">Special</label>
                                <input id="ts3" name="special" value="0" class="custom-checkbox" type="checkbox" hidden="hidden">
                                <label for="ts3" class="ts-helper"></label>
                            </div>
                        </div>
                        <div class="col-sm-6 m-b-20">
                            <div class="toggle-switch" data-ts-color="blue">
                                <label for="ts4" class="ts-label">Recommended</label>
                                <input id="ts4" name="recommended" value="0" class="custom-checkbox" type="checkbox" hidden="hidden">
                                <label for="ts4" class="ts-helper"></label>
                            </div>
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-save-file" data-form="form-add-items">Save</button>
                <button type="button" class="btn btn-link btn-cancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<!-- EDIT ITEM MODAL -->

<div class="modal fade in" id="modal-edit-items" data-modal-color="" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bgm-white">
                <h4 class="modal-title c-gray">Edit Item</h4>
            </div>
            <div class="modal-body bgm-white">
                {!! Form::open(['route' => ['items.update', ':UI'], 'method' => 'PUT', 'id' => 'form-edit-items', 'files' => 'true']) !!}
                    <div class="form-group fg-line m-b-20">
                        <input type="text" name="english_name-u" class="form-control input-sm" placeholder="Name (English)">
                    </div>
                    <small class="help-block c-red text-left" id="error-english_name-u"></small>

                    <div class="form-group fg-line m-b-20">
                        <input type="text" name="spanish_name-u" class="form-control input-sm" placeholder="Name (Spanish)">
                    </div>
                    <small class="help-block c-red text-left" id="error-spanish_name-u"></small>

                    <div class="form-group fg-line">
                        <input maxlength="20" type="text" name="position-u" class="form-control" placeholder="Position" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-position-u"></small>

                    <div class="form-group fg-line">
                        <input maxlength="20" type="text" name="price-u" class="form-control" placeholder="Price" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-price-u"></small>

                    <div class="form-group fg-line">
                        <input maxlength="20" type="text" name="offer-u" class="form-control" placeholder="Offer" autocomplete="off">
                    </div>
                    <small class="help-block c-red text-left" id="error-offer-u"></small>

                    <div class="form-group fg-line m-b-20 col-md-4">
                        <div class="thumbnail">
                            {!! Html::image('','Preview', ['class' => 'items-preview image-preview']) !!}
                        </div>
                    </div>

                    <div class="form-group fg-line m-b-20">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-primary btn-file m-r-10">
                                <span class="fileinput-new">Select Image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="image">
                            </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                        </div>
                    </div>
                    <small class="help-block c-red text-left" id="error-image"></small>

                    <div class="form-group fg-line m-b-20">
                        <textarea name="description-u" class="form-control auto-size input-sm" placeholder="Description"></textarea>
                    </div>
                    <small class="help-block c-red text-left" id="error-description-u"></small>

                    <div class="row">
                        <div class="col-sm-6 m-b-20">
                            <div class="toggle-switch" data-ts-color="green">
                                <label for="ts1-u" class="ts-label">New</label>
                                <input id="ts1-u" name="new-u" value="0" class="custom-checkbox" type="checkbox" hidden="hidden">
                                <label for="ts1-u" class="ts-helper"></label>
                            </div>
                        </div>
                        <div class="col-sm-6 m-b-20">
                            <div class="toggle-switch" data-ts-color="red">
                                <label for="ts2-u" class="ts-label">Spicy</label>
                                <input id="ts2-u" name="spicy-u" value="0" class="custom-checkbox" type="checkbox" hidden="hidden">
                                <label for="ts2-u" class="ts-helper"></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 m-b-20">
                            <div class="toggle-switch" data-ts-color="amber">
                                <label for="ts3-u" class="ts-label">Special</label>
                                <input id="ts3-u" name="special-u" value="0" class="custom-checkbox" type="checkbox" hidden="hidden">
                                <label for="ts3-u" class="ts-helper"></label>
                            </div>
                        </div>
                        <div class="col-sm-6 m-b-20">
                            <div class="toggle-switch" data-ts-color="blue">
                                <label for="ts4-u" class="ts-label">Recommended</label>
                                <input id="ts4-u" name="recommended-u" value="0" class="custom-checkbox" type="checkbox" hidden="hidden">
                                <label for="ts4-u" class="ts-helper"></label>
                            </div>
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-update-file" data-form="form-edit-items">Save</button>
                <button type="button" class="btn btn-link btn-cancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<!-- VIEW ITEM MODAL -->

<div class="modal fade in" id="modal-view-items" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body bgm-white p-20">

                <div class="card blog-post m-b-0">
                    <div class="bp-header">
                        <img src="img/widgets/preview.jpg" alt="" id="details-image">
                        <span class="bp-title bgm-lightgray">
                            <h2>
                                <span class="c-bluegray col-md-7 p-0">
                                    <span id="details-english_name"></span>
                                    <small id="details-spanish_name"></small>
                                </span>
                                <span class="c-green col-md-5 p-0 text-right">$<span id="details-price"></span></span>
                                <small id="details-observations">
                                </small>
                            </h2>
                        </span>
                    </div>
                    <div class="p-20">
                        <div class="c-gray" id="details-description">
                        </div>
                    </div>
                    <div class="p-10 c-gray text-right">
                          <span id="details-favorites">122</span> <i class="zmdi zmdi-favorite f-14 c-red"></i>
                          <span id="details-likes">232</span> <i class="zmdi zmdi-thumb-up f-14 c-blue"></i>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- VIEW USER MODAL -->

<div class="modal fade in" id="modal-view-users" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body bgm-white p-20">

                <div class="card profile-view m-b-0">
                    <div class="pv-header">
                        <img src="img/user.png" class="pv-main" id="details-image" alt="">
                    </div>

                    <div class="pv-body">
                        <h2 id="details-fullname"></h2>
                        <small id="details-address"></small>

                        <ul class="pv-follow c-gray">
                            <li><i class="zmdi zmdi-pin"></i> <span id="details-email"></span></li>
                            <li><i class="zmdi zmdi-phone"></i> <span id="details-phone_number"></span></li>
                        </ul>
                        <button data-dismiss="modal" class="btn bgm-red btn-icon waves-effect waves-circle waves-float"><i class="zmdi zmdi-close"></i></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<!-- EDIT CATEGORY MODAL -->

<div class="modal fade in" id="modal-edit-events" data-modal-color="" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bgm-white">
                <h4 class="modal-title c-gray">Edit Event</h4>
            </div>
            <div class="modal-body bgm-white">
                {!! Form::open(['route' => ['events.update', ':UI'], 'method' => 'PUT', 'id' => 'form-edit-events', 'files' => 'true']) !!}
                    <div class="form-group fg-line m-b-0">
                        <input type="text" name="date-u" class="form-control date-picker" placeholder="Date" autocomplete="off" value="">
                    </div>
                    <small class="help-block c-red text-left" id="error-date-u"></small>

                    <div class="form-group fg-line m-b-20">
                        <input type="text" name="name-u" class="form-control input-sm" placeholder="Name (English)">
                    </div>
                    <small class="help-block c-red text-left" id="error-name-u"></small>

                    <div class="form-group fg-line m-b-0">
                        <select name="subsidiary_id-u" class="selectpicker subsidiaries-select" title='SUBSIDIARY'>
                        </select>
                    </div>
                    <small class="help-block c-red text-left" id="error-subsidiary_id-u"></small>

                    <div class="form-group fg-line m-b-20 col-md-4">
                        <div class="thumbnail">
                            {!! Html::image('','Preview', ['class' => 'events-preview image-preview']) !!}
                        </div>
                    </div>

                    <div class="form-group fg-line m-b-20">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-primary btn-file m-r-10">
                                <span class="fileinput-new">Select Image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="image">
                            </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                        </div>
                    </div>
                    <small class="help-block c-red text-left" id="error-image"></small>

                    <div class="form-group fg-line m-b-20">
                        <textarea name="details-u" class="form-control auto-size input-sm" placeholder="Description"></textarea>
                    </div>
                    <small class="help-block c-red text-left" id="error-details-u"></small>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-update-file" data-form="form-edit-events">Save</button>
                <button type="button" class="btn btn-link btn-cancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>










<!-- FORM TO DESTROY RESOURCES -->

{!! Form::open(['route' => ['users.destroy', ':UI'], 'method' => 'DELETE', 'id' => 'form-delete-users']) !!}
{!! Form::close() !!}

{!! Form::open(['route' => ['reservations.destroy', ':UI'], 'method' => 'DELETE', 'id' => 'form-delete-reservations']) !!}
{!! Form::close() !!}

{!! Form::open(['route' => ['categories.destroy', ':UI'], 'method' => 'DELETE', 'id' => 'form-delete-categories']) !!}
{!! Form::close() !!}

{!! Form::open(['route' => ['items.destroy', ':UI'], 'method' => 'DELETE', 'id' => 'form-delete-items']) !!}
{!! Form::close() !!}

{!! Form::open(['route' => ['events.destroy', ':UI'], 'method' => 'DELETE', 'id' => 'form-delete-events']) !!}
{!! Form::close() !!}