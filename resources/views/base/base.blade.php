<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title id="navigator-title">{{ $company->name }}</title>

        <link rel="shortcut icon" href="images/ico/favicon1.png">

        <!-- CSS -->
        {!! Html::style('css/admin.css') !!}

    </head>
    <body>

        <header id="header" class="clearfix" data-current-skin="red">
            <ul class="header-inner">
                <li id="menu-trigger" data-trigger="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>

                <li class="logo hidden-xs">
                    <a href="" class="title-custom">{{ $company->name }}</a>
                </li>

                <li class="pull-right">
                    <ul class="top-menu">
                        <li id="toggle-width">
                            <div class="toggle-switch">
                                <input id="tw-switch" type="checkbox" hidden="hidden">
                                <label for="tw-switch" class="ts-helper"></label>
                            </div>
                        </li>

                        <!--<li id="top-search">
                            <a href=""><i class="tm-icon zmdi zmdi-search"></i></a>
                        </li>-->

                        <li data-view="reservations">
                            <a href="#" class="view">
                                <i class="tm-icon zmdi zmdi-notifications"></i>
                                <i class="tmn-counts bgm-deeporange invisible" id="new-requests"></i>
                            </a>
                        </li>

                        <li class="dropdown">
                            <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                            <ul class="dropdown-menu dm-icon pull-right">
                                <li class="hidden-xs">
                                    <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a>
                                </li>
                                <li>
                                    <a href="{{ url('/logout') }}"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>

            <!-- Top Search Content -->
            <div id="top-search-wrap">
                <div class="tsw-inner">
                    <i id="top-search-close" class="zmdi zmdi-arrow-left"></i>
                    <input type="text">
                </div>
            </div>
        </header>

        <section id="main" data-layout="layout-1" style="padding-bottom:50px;">
            <aside id="sidebar" class="sidebar c-overflow">
                <div class="profile-menu">
                    <a href="">
                        <div class="profile-pic">
                            {!! Html::image(Auth::user()->image == NULL ? 'img/user.png' : Auth::user()->image, 'avatar', ['class' => 'img-profile']) !!}
                        </div>

                        <div class="profile-info">
                            <span class="title-settings">{{ Auth::user()->fullname }}  <b>·</b>  {{ Auth::user()->role }}</span>
                            <input type="hidden" id="welcome-message" value="{{ Auth::user()->first_name }}">
                            <i class="zmdi zmdi-caret-down"></i>
                        </div>
                    </a>

                    <ul class="main-menu">
                        <li data-view="settings"  class="nav-btn"><a href="#"  data-function="" class="view"><i class="zmdi zmdi-settings"></i> Settings</a></li>

                        @if( Auth::user()->role == 'root' )
                        <li data-view="analytics"  class="nav-btn"><a href="#" data-function="" class="view"><i class="zmdi zmdi-trending-up"></i> Analytics</a></li>
                        <li data-view="startup"    class="nav-btn"><a href="#" data-function="" class="view"><i class="zmdi zmdi-fire"></i> Startup Settings</a></li>
                        @endif

                        <li><a href="{{ url('/logout') }}"><i class="zmdi zmdi-time-restore"></i> Logout</a></li>
                    </ul>
                </div>

                <ul class="main-menu">
                    <li data-view="home"            class="nav-btn active"><a href="#" data-function="" class="view"><i class="zmdi zmdi-home"></i> Home</a></li>
                    @if( Auth::user()->role == 'root' || Auth::user()->role == 'administrator' )
                    <li data-view="users"           class="nav-btn"><a href="#" data-function="" class="view"><i class="zmdi zmdi-accounts-alt"></i> Users</a></li>
                    @endif
                    <li data-view="reservations"    class="nav-btn"><a href="#" data-function="" class="view"><i class="zmdi zmdi-bookmark"></i> Reservations</a></li>
                    <li data-view="categories"      class="nav-btn"><a href="#" data-function="" class="view"><i class="zmdi zmdi-cutlery"></i> Menu</a></li>
                    <li data-view="events"          class="nav-btn"><a href="#" data-function="" class="view"><i class="zmdi zmdi-star"></i> Events</a></li>
                </ul>
            </aside>

            <section id="content">
                <div class="container">

                    @yield('content')

                </div>
            </section>

        </section>

        <footer id="footer" style="padding-top: 50px; padding-bottom: 0px;">&copy; 2015 Pelancho's inc. All rights reserved.</footer>

        <div class="page-loader">
            <div class="preloader pls-blue">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Please wait...</p>
            </div>
        </div>

        <!-- Javascript -->
        {!! Html::script('js/admin.js') !!}
        @yield('scripts')

        <audio src="audio/notification.mp3" id="audio-notification">
        </audio>

    </body>
  </html>